<?php

class ProyectoController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('view', 'update', 'index', 'deleteUsuario'),
                'roles' => array('cliente', 'empleado'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('view', 'update', 'index', 'admin', 'delete', 'create'),
                'roles' => array('administrador'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $model = $this->loadModel($id);
        if (!Yii::app()->user->checkAccess('usuarioVinculadoProyecto', array('Proyecto' => $model))) {
            throw new CHttpException('404','El usuario no esta vinculado al Proyecto');
        }

        $usuarioProyectoFuncion = $this->nuevoUsuario($model);
        $dataproviderUsuarios= new UsuarioProyectoFuncion('search');
        $dataproviderUsuarios->idProyecto=$id;
        $dataproviderUsuarios=$dataproviderUsuarios->search();
        $dataproviderUsuarios->pagination=false;
        $this->render('view', array(
            'model' => $model,
            'modelUsuarioProyectoFuncion' => $usuarioProyectoFuncion,
            'dataproviderUsuarios'=>$dataproviderUsuarios
        ));
    }

    /**
     * Creates a new comment.
     * This method attempts to create a new comment based on the user input.
     * If the comment is successfully created, the browser will be redirected
     * to show the created comment.
     * @param Proyecto $proyecto
     * @return UsuarioProyectoFuncion 
     */
    protected function nuevoUsuario($proyecto) {

        $usuarioProyectoFuncion = new UsuarioProyectoFuncion;

        if (isset($_POST['UsuarioProyectoFuncion'])) {
            $usuarioProyectoFuncion->attributes = $_POST['UsuarioProyectoFuncion'];
            $usuarioProyectoFuncion->idProyecto = $proyecto->id;
             if (!Yii::app()->user->checkAccess('usuarioVinculadoProyecto', array('Proyecto' => $proyecto, 'Funcion' => Proyecto::$LIDERPROYECTO))) {
                    throw new CHttpException('404', 'El usuario no es Lider de Proyecto');
                }
            if ($usuarioProyectoFuncion->findByAttributes($usuarioProyectoFuncion->getAttributes())) {
                $usuarioProyectoFuncion->addErrors(array('idProyecto' => 'El usuario ya existe con esa funcion en el Proyecto'));
            } else {
               
                if ($usuarioProyectoFuncion->save()) {
                    Yii::app()->user->setFlash('usuarioProyecto', 'El Usuario ha sido agregado');
                    $this->refresh();
                }
            }
        }
        return $usuarioProyectoFuncion;
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Proyecto;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Proyecto'])) {
            $model->attributes = $_POST['Proyecto'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        if (!Yii::app()->user->checkAccess('usuarioVinculadoProyecto', array('Proyecto' => $model, 'Funcion' => Proyecto::$LIDERPROYECTO))) {
            throw new CHttpException('404','El usuario no esta vinculado al Proyecto');
        }
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Proyecto'])) {
            $model->attributes = $_POST['Proyecto'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionDeleteUsuario($idProyecto, $idFuncion, $idUsuario) {
        $model = $this->loadModel($idProyecto);
        if (!Yii::app()->user->checkAccess('usuarioVinculadoProyecto', array('Proyecto' => $model, 'Funcion' => Proyecto::$LIDERPROYECTO))) {
            throw new CHttpException('404', 'El usuario no es Lider de Proyecto');
        }
        $usuarioProyectoFuncion = UsuarioProyectoFuncion::model()->find('idProyecto=:idProyecto and idFuncion=:idFuncion and idUsuario=:idUsuario', array(':idProyecto' => $idProyecto, ':idFuncion' => $idFuncion, ':idUsuario' => $idUsuario));
        $usuarioProyectoFuncion->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Proyecto');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Proyecto('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Proyecto']))
            $model->attributes = $_GET['Proyecto'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Proyecto the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Proyecto::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Proyecto $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'proyecto-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
