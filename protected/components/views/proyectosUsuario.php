<?php
/* @var $proyectosUsuario UsuarioProyectoFuncion[] */
?>
<?php
$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Proyectos Asignados',
		));?>

        <ul class="operations">
            <?php foreach ($proyectosUsuario as $proyectoFuncion): ?>
                <li>
                    <?php echo CHtml::link(CHtml::encode($proyectoFuncion->idProyecto0->Proyecto), array('/proyecto/view', 'id' => $proyectoFuncion->idProyecto)); ?>
                </li>
            <?php endforeach; ?>
        </ul>
<?php $this->endWidget();?>