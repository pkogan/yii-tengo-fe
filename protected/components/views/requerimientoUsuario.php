<?php
/* @var $requerimientoUsuario UsuariosAsignados[] */
?>
<?php
$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Requerimientos Asignados',
		));?>

        <ul class="operations">
            <?php 
            	if($requerimientoUsuario==null)
            		echo 'No tiene requerimientos';
            	else {
            		foreach ($requerimientoUsuario as $requerimiento): ?>
                <li>
                    <?php echo CHtml::link('Requerimiento '.CHtml::encode($requerimiento->idRequerimientos), array('/requerimientos/requerimientos/view', 'id' => $requerimiento->idRequerimientos)); ?>
                </li>
            <?php endforeach; }?>
        </ul>
<?php $this->endWidget();?>