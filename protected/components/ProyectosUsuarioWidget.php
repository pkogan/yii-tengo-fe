<?php

class ProyectosUsuarioWidget extends CWidget
{
    public function init()
    {
        //echo 'inicio';
       
        // this method is called by CController::beginWidget()
    }
 
    public function run()
    {
        
        //echo 'fin';
        // this method is called by CController::endWidget()
        if(!Yii::app()->user->isGuest){
            $usuario= Usuario::model()->findByPk(Yii::app()->user->getState('idUsuario'));
            /*@var $usuario Usuario*/
            $this->render('proyectosUsuario',array('proyectosUsuario'=>$usuario->usuarioProyectoFuncions));
        }
        
    }
}
