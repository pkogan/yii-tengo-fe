<?php
/* @var $this IncidentesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Proyecto'=>array('/proyecto/view', 'id'=>$_GET['id']),
    'Incidentes',
);

$this->menu=array(
	//array('label'=>'Create Incidentes', 'url'=>array('/incidentes/logincidentes/create', 'id'=>$model->idIncidente)),
	array('label'=>'Crear Incidente', 'url'=>array('create', 'id'=>$_GET['id'])),
	array('label'=>'Administrar Incidentes', 'url'=>array('admin')),
);
?>

<h1>Incidentes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 
?>
