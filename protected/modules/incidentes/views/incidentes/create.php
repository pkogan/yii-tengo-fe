<?php
/* @var $this IncidentesController */
/* @var $model Incidentes */

$this->breadcrumbs=array(
	'Incidentes'=>array('index', 'id'=>$model->idProyecto),
	'Crear',
);
/*
$this->menu=array(
	array('label'=>'List Incidentes', 'url'=>array('index')),
	array('label'=>'Manage Incidentes', 'url'=>array('admin')),
);*/
?>

<h1>Crear Incidente</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>