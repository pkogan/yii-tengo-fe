<?php
/* @var $this IncidentesController */
/* @var $model Incidentes */

$this->breadcrumbs=array(
	$model->idProyecto0->Proyecto=>array('/proyecto/view', 'id'=>$model->idProyecto),
        'Incidentes'=>array('index', 'id'=>$model->idProyecto),
	
);

$this->menu=array(
	//array('label'=>'Crear Log', 'url'=>array('/incidentes/logincidentes/create', 'id'=>$model->idIncidente)),
	array('label'=>'Actualizar Incidente', 'url'=>array('update', 'id'=>$model->idIncidente)),
	array('label'=>'Borrar Incidente', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idIncidente),'confirm'=>'Desea Borrar este incidente?')),
	//array('label'=>'Administrar Incidentes', 'url'=>array('admin')),
);
?>

<h1><?php echo $model->nombre; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'descripcion',
                'idProyecto0.Proyecto',
                'fechaAlta',
		'idSeveridad0.severidad',
		'idTipo0.tipo',
                'idUsuario0.Usuario',
	),
)); ?>

<br/><br/><br/>
<h2>Log Incidentes</h2>

<?php if (Yii::app()->user->hasFlash('nuevoLog')): ?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('nuevoLog'); ?>
    </div>
<?php endif; ?>
<script>
    function crearLog(){
        var a=document.getElementById('newLog');
        if(a.style.display=='none')
            a.style.display='block';
        else
            a.style.display='none'
    }
</script>
<?php echo CHtml::link('Crear Log', '#', array('onclick' => 'crearLog();'));?>

<div id="newLog" class="usuarios-form search-form" style="display:none;">
    <?php $this->renderPartial('_form-log', array('model' => $incidentes)) ?>
</div>




<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'proyecto-grid',
    'dataProvider' => $dataproviderLog,
    'columns' => array(
        'idUsuario0.Usuario',
        'idEstado0.descripcion',
        'descripcion',
        'fecha',
                
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete} {update}',
            'buttons' => array(
                'delete' => array(
                   'url' => 'Yii::app()->controller->createUrl(\'logincidentes/delete\', array(\'id\'=>$data["idLog"]))',
                ),
                'update'=>array(
                    'url'=>'Yii::app()->controller->createUrl(\'logincidentes/update\', array(\'id\'=>$data["idLog"]))',
                ),
            ),
        ),
    ),
));
?>