<?php
/* @var $this LogincidentesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Logincidentes',
);

$this->menu=array(
	array('label'=>'Create Logincidentes', 'url'=>array('create')),
	array('label'=>'Manage Logincidentes', 'url'=>array('admin')),
);
?>

<h1>Logincidentes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
