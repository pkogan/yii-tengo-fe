<?php
/* @var $this LogincidentesController */
/* @var $model Logincidentes */

$this->breadcrumbs=array(
	'Logincidentes'=>array('index'),
	$model->idLog,
);

$this->menu=array(
	array('label'=>'List Logincidentes', 'url'=>array('index')),
	array('label'=>'Create Logincidentes', 'url'=>array('create')),
	array('label'=>'Update Logincidentes', 'url'=>array('update', 'id'=>$model->idLog)),
	array('label'=>'Delete Logincidentes', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idLog),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Logincidentes', 'url'=>array('admin')),
);
?>

<h1>View Logincidentes #<?php echo $model->idLog; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idLog',
		'idIncidente',
		'fecha',
		'idUsuario',
		'descripcion',
		'idEstado',
	),
)); ?>
