<?php
/* @var $this LogincidentesController */
/* @var $model Logincidentes */

$this->breadcrumbs=array(
	'Logincidentes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Logincidentes', 'url'=>array('index')),
	array('label'=>'Manage Logincidentes', 'url'=>array('admin')),
);
?>

<h1>Create Logincidentes</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>