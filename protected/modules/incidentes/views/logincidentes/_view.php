<?php
/* @var $this LogincidentesController */
/* @var $data Logincidentes */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idLog')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idLog), array('view', 'id'=>$data->idLog)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idIncidente')); ?>:</b>
	<?php echo CHtml::encode($data->idIncidente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idUsuario')); ?>:</b>
	<?php echo CHtml::encode($data->idUsuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idEstado')); ?>:</b>
	<?php echo CHtml::encode($data->idEstado); ?>
	<br />


</div>