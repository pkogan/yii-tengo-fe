<?php
/* @var $this LogincidentesController */
/* @var $model Logincidentes */

$this->breadcrumbs=array(
	$model->idIncidente0->nombre=>array('incidentes/view', 'id'=>$model->idIncidente),
	'Actualizar '.$model->descripcion,
);
/*
$this->menu=array(
	array('label'=>'List Logincidentes', 'url'=>array('index')),
	array('label'=>'Create Logincidentes', 'url'=>array('create')),
	array('label'=>'View Logincidentes', 'url'=>array('view', 'id'=>$model->idLog)),
	array('label'=>'Manage Logincidentes', 'url'=>array('admin')),
);*/
?>

<h1>Actualizar Log</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>