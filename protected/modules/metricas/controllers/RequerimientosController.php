<?php

class RequerimientosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('pepe','index','view','reportePDF'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','reportePDF'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','reportePDF'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        public function actionPepe($id){
             if (isset($id)) {
           $dataProvider=new CActiveDataProvider('RequerimientosMetricas', array(
            'criteria'=>array(
            'condition'=>'idProyectoRequerimiento = :Requerimientos',
            'params'=>array(':Requerimientos'=>$id),
            )));
         } else    
         {
             $dataProvider=new CActiveDataProvider('RequerimientosMetricas');
         }
     $this->render('index',array('dataProvider'=>$dataProvider,'idProyecto'=>$id,'model'=>$this->loadModelProyecto($id)));       
 
            //$this->render('pepe');
        }
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$model=new RequerimientosMetricas();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RequerimientosMetricas']))
		{
			$model->attributes=$_POST['RequerimientosMetricas'];
                        $model->idProyectoRequerimiento=$id;
                        $idUsuarioRequerimiento=Yii::app()->user->getState('idUsuario');
                        $model->idUsuarioRequerimiento=$idUsuarioRequerimiento;
			if($model->save())
				$this->redirect(array('view','id'=>$model->idRequerimientos,'idProyecto'=>$model->idProyectoRequerimiento));
		}

		$this->render('create',array(
			'model'=>$model,'id'=>$id
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id,$idProyecto)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RequerimientosMetricas']))
		{
                      
			$model->attributes=$_POST['RequerimientosMetricas'];
                        $idUsuarioRequerimiento=Yii::app()->user->getState('idUsuario');
                        $model->idUsuarioRequerimiento=$idUsuarioRequerimiento;
                       
			if($model->save())
				$this->redirect(array('view','id'=>$model->idRequerimientos));
		}
                
		$this->render('update',array(
			'model'=>$model,'idProyecto'=>$model->idProyectoRequerimiento,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id)
	{
         /*   
		$dataProvider=new CActiveDataProvider('Requerimientos');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
  
        } */
  
          //exit($id);  
	           
         if (isset($id)) {
             
            $dataProvider=new CActiveDataProvider('RequerimientosMetricas', array(
            'criteria'=>array(
            'condition'=>'idProyectoRequerimiento = :Requerimientos',
            'params'=>array(':Requerimientos'=>$id),
            )));
         }

     $this->render('index',array(
			'dataProvider'=>$dataProvider,'idProyecto'=>$id,
                        'model'=>$this->loadModelProyecto($id),
		));       
 
	}

     
        
	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{
		$model=new RequerimientosMetricas('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['RequerimientosMetricas']))
			$model->attributes=$_GET['Requerimientos'];
                $model->idProyectoRequerimiento=$id;
		$this->render('admin',array(
			'model'=>$model,'idProyecto'=>$id
		));
	}

             public function actionReportePDF($id)
        {
                 
            //exit($id);        
            if (isset($id)) {
                
                $criteria = new CDbCriteria;
                $criteria->condition='idProyectoRequerimiento = :Requerimientos';
                $criteria->params=array(':Requerimientos'=>$id);
                $dataProvider = RequerimientosMetricas::model()->findAll($criteria);
                
                
   
            }
            $model=$this->loadModelProyecto($id);
    //        echo "en actionreportepdf";
            //exit((string)count($dataProvider).'lllllllll');
            $this->render('reportepdf',array( 
                     'model'=>$model,
                     'dataProvider'=>$dataProvider,
                
                
            ));   
        }         
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return RequerimientosMetricas the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=  RequerimientosMetricas::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

public function loadModelProyecto($id)
	{       //echo "model del proyecto";
                //exit($id);    
		$model= ProyectoMetricas::model()->findByPk($id);

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        /**
	 * Performs the AJAX validation.
	 * @param RequerimientosMetricas $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='requerimientos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
