<?php
/* @var $this EstadorequerimientoController */
/* @var $model Estadorequerimiento */

$this->breadcrumbs=array(
	'Estadorequerimientos'=>array('index'),
	$model->idEstado,
);

$this->menu=array(
	array('label'=>'List Estadorequerimiento', 'url'=>array('index')),
	array('label'=>'Create Estadorequerimiento', 'url'=>array('create')),
	array('label'=>'Update Estadorequerimiento', 'url'=>array('update', 'id'=>$model->idEstado)),
	array('label'=>'Delete Estadorequerimiento', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idEstado),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Estadorequerimiento', 'url'=>array('admin')),
);
?>

<h1>View Estadorequerimiento #<?php echo $model->idEstado; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idEstado',
		'DescripcionEstado',
	),
)); ?>
