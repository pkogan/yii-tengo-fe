<?php
/* @var $this EstadorequerimientoController */
/* @var $model Estadorequerimiento */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'estadorequerimiento-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'DescripcionEstado'); ?>
		<?php echo $form->textField($model,'DescripcionEstado',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'DescripcionEstado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->