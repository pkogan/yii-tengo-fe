<?php
/* @var $this EstadorequerimientoController */
/* @var $model Estadorequerimiento */

$this->breadcrumbs=array(
	'Estadorequerimientos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Estadorequerimiento', 'url'=>array('index')),
	array('label'=>'Manage Estadorequerimiento', 'url'=>array('admin')),
);
?>

<h1>Create Estadorequerimiento</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>