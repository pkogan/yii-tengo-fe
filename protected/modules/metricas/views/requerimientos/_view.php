<?php
/* @var $this RequerimientosController */
/* @var $data Requerimientos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idRequerimientos')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idRequerimientos), array('view', 'id'=>$data->idRequerimientos)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombreRequerimiento')); ?>:</b>
	<?php echo CHtml::encode($data->nombreRequerimiento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actores')); ?>:</b>
	<?php echo CHtml::encode($data->actores); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('costoReal')); ?>:</b>
	<?php echo CHtml::encode($data->costoReal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosUtilizados')); ?>:</b>
	<?php echo CHtml::encode($data->datosUtilizados); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DescripcionComplejidad')); ?>:</b>
	<?php echo CHtml::encode($data->idComplejidadRequerimiento0->descripcionComplejidad); ?>
	<br />

 
	<b><?php echo CHtml::encode($data->getAttributeLabel('DescripcionEstado')); ?>:</b>
	<?php echo CHtml::encode($data->idEstadoRequerimiento0->DescripcionEstado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Usuario')); ?>:</b>
	<?php echo CHtml::encode($data->idUsuarioRequerimiento0->Usuario); ?>
	<br />

<!--
        <b><?php //echo CHtml::encode('Costo Estimado Req. Hs.');?>:</b>
           <?php //echo CHtml::encode($data->costoEstimadoReqHoras()) ;?>
        <br/>
  
        <b><?php //echo CHtml::encode('Costo Estimado Proy. Hs.');?>:</b>
           <?php //echo CHtml::encode($data->idProyectoRequerimiento0->costoEstimadoProyHoras()) ;?>
         <br/>
       <b><?php //echo CHtml::encode('Duración Proy. en Semanas');?>:</b>
           <?php //echo CHtml::encode($data->idProyectoRequerimiento0->duracionProySemanas()) ;?>
         <br/>
-->

</div>
