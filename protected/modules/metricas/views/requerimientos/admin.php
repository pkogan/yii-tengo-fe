<?php
/* @var $this RequerimientosController */
/* @var $model Requerimientos */

$this->breadcrumbs=array(
	'Requerimientos'=>array('index','id'=>$idProyecto),
	'Manage',
);

$this->menu=array(
   
	array('label'=>'List Requerimientos', 'url'=>array('index','id'=>$idProyecto),
	array('label'=>'Create Requerimientos', 'url'=>array('create', 'id'=>$idProyecto)),
        array('label'=>'Actualizar Requerimientos', 'url'=>array('update','id'=>$idProyecto)),            
            ),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#requerimientos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administración de Requerimientos</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,'idProyecto'=>$idProyecto,
)); ?>
</div><!-- search-form -->

<div align="right" class="row">

</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'requerimientos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'htmlOptions'=>array('style'=>'word-wrap:break-word; width:900px;'),
     
	'columns'=>array(
		'idRequerimientos',
		'nombreRequerimiento',
		'actores',
		'descripcion',
		'costoReal',
		'datosUtilizados',
                array(
		'name'=>'idComplejidadRequerimiento0.descripcionComplejidad',
		'value'=>'$data->idComplejidadRequerimiento0->descripcionComplejidad',
		
	     ),            
    
		'idUsuarioRequerimiento0.Usuario',
            	'idProyectoRequerimiento0.Proyecto',
               array(
		'name'=>'Costo Estimado Req. Horas',
		'value'=>'$data->costoEstimadoReqHoras()',
		
	     ),            

                array(
		'name'=>'idProyectoRequerimiento0.duracionProySemanasmanas',
		'value'=>'$data->idProyectoRequerimiento0->duracionProySemanas()',
		
	     ),             
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>


