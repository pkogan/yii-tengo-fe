<?php
/* @var $this RequerimientosController */
/* @var $model Requerimientos */

$this->breadcrumbs=array(
	'Requerimientos'=>array('index','id'=>$model->idProyectoRequerimiento),
	$model->idRequerimientos,
);
$this->menu=array(
	array('label'=>'Crear Requerimientos', 'url'=>array('create','id'=>$model->idProyectoRequerimiento)),
        array('label'=>'Actualizar Requerimientos', 'url'=>array('update','id'=>$model->idRequerimientos,'idProyecto'=>$model->idProyectoRequerimiento)),

);


?>

<h1>View Requerimientos #<?php echo $model->nombreRequerimiento; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idRequerimientos',
		'nombreRequerimiento',
		'actores',
		'descripcion',
		'costoReal',
		'datosUtilizados',
     		'idEstadoRequerimiento0.DescripcionEstado',

		'idComplejidadRequerimiento0.descripcionComplejidad',
//		'idEstadoRequerimiento',
//		'idUsuarioRequerimiento',
		'idUsuarioRequerimiento0.Usuario',
            	'idProyectoRequerimiento0.Proyecto',
                
	),
)); ?>

            
