<?php
/* @var $this RequerimientosController */
/* @var $model Requerimientos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'requerimientos-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombreRequerimiento'); ?>
		<?php echo $form->textField($model,'nombreRequerimiento',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nombreRequerimiento'); ?>
	</div>

<!--
        <div class="row">
		<?php echo $form->labelEx($model,'actores'); ?>
		<?php echo $form->textField($model,'actores',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'actores'); ?>
	</div>
-->
	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'costoReal'); ?>
		<?php echo $form->textField($model,'costoReal'); ?>
		<?php echo $form->error($model,'costoReal'); ?>
	</div>
<!--
	<div class="row">
		<?php echo $form->labelEx($model,'datosUtilizados'); ?>
		<?php echo $form->textField($model,'datosUtilizados',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'datosUtilizados'); ?>
	</div>
-->
        
     

        <div>

            <label>Complejidad</label>
            <?php
            
                echo $form->dropDownList($model,'idComplejidadRequerimiento',CHtml::listData(Complejidad::model()->findAll(),'idComplejidad','descripcionComplejidad'), array('class'=>'span6'));?>
            <?php echo $form->error($model,'idComplejidadRequerimiento'); ?>
    
        </div>
       <div class="row">
            <label>Estado</label>
            <?php echo $form->dropDownList($model,'idEstadoRequerimiento', CHtml::listData(Estadorequerimiento::model()->findall(),'idEstado','DescripcionEstado'),array('class'=>'span6')) ?>
                 <?php echo $form->error($model,'idEstadoRequerimiento'); ?>
       </div>    

<!--

	<div class="row">
		<?php echo $form->labelEx($model,'idUsuarioRequerimiento'); ?>
		<?php echo $form->textField($model,'idUsuarioRequerimiento'); ?>
		<?php echo $form->error($model,'idUsuarioRequerimiento'); ?>
	</div>
-->
<!--
<div class="row">
faltaria poner tomar el idproyecto que he seleccionado cuando inicie, para guardar
		<?php echo $form->labelEx($model,'idProyectoRequerimiento'); ?>
		<?php echo $form->textField($model,'idProyectoRequerimiento'); ?>
		<?php echo $form->error($model,'idProyectoRequerimiento'); ?>

	</div>
-->
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->