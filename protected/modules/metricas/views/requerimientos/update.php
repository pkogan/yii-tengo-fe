<?php
/* @var $this RequerimientosController */
/* @var $model Requerimientos */

$this->breadcrumbs=array(
	'Requerimientos'=>array('index','idProyecto'=>$idProyecto),
	$model->idRequerimientos=>array('view','id'=>$model->idRequerimientos),
	'Update',
);


?>

<h1>Update Requerimientos <?php echo $model->idRequerimientos; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'idProyecto'=>$idProyecto)); ?>