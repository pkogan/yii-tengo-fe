<?php
/* @var $this RequerimientosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Requerimientos',
);

$this->menu=array(
	array('label'=>'Crear Requerimientos', 'url'=>array('create','id'=>$idProyecto)),
        array('label'=>'Manage Requerimientos', 'url'=>array('admin','id'=>$idProyecto)),
        array('label'=>'Exportar a ⇒ PDF', 'url'=> array('reportePDF','id'=>$idProyecto)),
        
);

?>

<h1>Métricas de <?php echo $model->Proyecto ?></h1>
<?php
$ceph =$model->FechaFin;
$ceph =$model->costoEstimadoProyHoras();
 echo '<b>Costo Estimado Proyecto en Horas=</b>'.$ceph;
 echo '<br />';
 
 $cepp=$model->ValorHora*$ceph;
 echo '<b>Costo Estimado Proyecto en Pesos=</b>'.$cepp;
 echo '<br />';
 echo '<b>Duración Estimada del Proyecto en Semanas=</b>'.round($model->duracionProySemanas(),2);
 echo '<br />';
  $crph=$model->costoRealProyHoras();
 echo '<b>Costo Real Proyecto en Horas=</b>'.$crph;
 echo '<br />';
 $crpp=$crph*$model->ValorHora;
 echo '<b>Costo Real Proyecto en Pesos=</b>'.$crpp;
 echo '<br /><br /><br />';

?>
 


<h1>Requerimientos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    
)); ?>


