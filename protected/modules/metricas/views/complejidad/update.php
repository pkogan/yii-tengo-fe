<?php
/* @var $this ComplejidadController */
/* @var $model Complejidad */

$this->breadcrumbs=array(
	'Complejidads'=>array('index'),
	$model->idComplejidad=>array('view','id'=>$model->idComplejidad),
	'Update',
);

$this->menu=array(
	array('label'=>'List Complejidad', 'url'=>array('index')),
	array('label'=>'Create Complejidad', 'url'=>array('create')),
	array('label'=>'View Complejidad', 'url'=>array('view', 'id'=>$model->idComplejidad)),
	array('label'=>'Manage Complejidad', 'url'=>array('admin')),
);
?>

<h1>Update Complejidad <?php echo $model->idComplejidad; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>