<?php
/* @var $this ComplejidadController */
/* @var $data Complejidad */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idComplejidad')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idComplejidad), array('view', 'id'=>$data->idComplejidad)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcionComplejidad')); ?>:</b>
	<?php echo CHtml::encode($data->descripcionComplejidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('puntos')); ?>:</b>
	<?php echo CHtml::encode($data->puntos); ?>
	<br />


</div>