<?php
/* @var $this ComplejidadController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Complejidads',
);

$this->menu=array(
	array('label'=>'Create Complejidad', 'url'=>array('create')),
	array('label'=>'Manage Complejidad', 'url'=>array('admin')),
);
?>

<h1>Complejidads</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
