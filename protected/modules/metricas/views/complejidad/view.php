<?php
/* @var $this ComplejidadController */
/* @var $model Complejidad */

$this->breadcrumbs=array(
	'Complejidads'=>array('index'),
	$model->idComplejidad,
);

$this->menu=array(
	array('label'=>'List Complejidad', 'url'=>array('index')),
	array('label'=>'Create Complejidad', 'url'=>array('create')),
	array('label'=>'Update Complejidad', 'url'=>array('update', 'id'=>$model->idComplejidad)),
	array('label'=>'Delete Complejidad', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idComplejidad),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Complejidad', 'url'=>array('admin')),
);
?>

<h1>View Complejidad #<?php echo $model->idComplejidad; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idComplejidad',
		'descripcionComplejidad',
		'puntos',
	),
)); ?>
