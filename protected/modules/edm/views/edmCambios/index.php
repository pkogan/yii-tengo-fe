<?php
/* @var $this EdmCambiosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Edm Cambioses',
);

$this->menu=array(
	array('label'=>'Create EdmCambios', 'url'=>array('create')),
	array('label'=>'Manage EdmCambios', 'url'=>array('admin')),
);
?>

<h1>Edm Cambioses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
