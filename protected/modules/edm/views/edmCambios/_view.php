<?php
/* @var $this EdmCambiosController */
/* @var $data EdmCambios */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('cambio_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->cambio_id), array('view', 'id'=>$data->cambio_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cambio_documento')); ?>:</b>
	<?php echo CHtml::encode($data->cambio_documento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cambio_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->cambio_nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cambio_archivo')); ?>:</b>
	<?php echo CHtml::encode($data->cambio_archivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cambio_fechayhora')); ?>:</b>
	<?php echo CHtml::encode($data->cambio_fechayhora); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cambio_usuario')); ?>:</b>
	<?php echo CHtml::encode($data->cambio_usuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cambio_privilegios')); ?>:</b>
	<?php echo CHtml::encode($data->cambio_privilegios); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cambio_descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->cambio_descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cambio_activo')); ?>:</b>
	<?php echo CHtml::encode($data->cambio_activo); ?>
	<br />

	*/ ?>

</div>