<?php
/* @var $this EdmCambiosController */
/* @var $model EdmCambios */

$this->breadcrumbs=array(
	'Edm Cambioses'=>array('index'),
	$model->cambio_id=>array('view','id'=>$model->cambio_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EdmCambios', 'url'=>array('index')),
	array('label'=>'Create EdmCambios', 'url'=>array('create')),
	array('label'=>'View EdmCambios', 'url'=>array('view', 'id'=>$model->cambio_id)),
	array('label'=>'Manage EdmCambios', 'url'=>array('admin')),
);
?>

<h1>Update EdmCambios <?php echo $model->cambio_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>