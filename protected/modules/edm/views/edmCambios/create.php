<?php
/* @var $this EdmCambiosController */
/* @var $model EdmCambios */

$this->breadcrumbs=array(
	'Edm Cambioses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EdmCambios', 'url'=>array('index')),
	array('label'=>'Manage EdmCambios', 'url'=>array('admin')),
);
?>

<h1>Create EdmCambios</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>