<?php
/* @var $this EdmCambiosController */
/* @var $model EdmCambios */

$this->breadcrumbs=array(
	'Edm Cambioses'=>array('index'),
	$model->cambio_id,
);

$this->menu=array(
	array('label'=>'List EdmCambios', 'url'=>array('index')),
	array('label'=>'Create EdmCambios', 'url'=>array('create')),
	array('label'=>'Update EdmCambios', 'url'=>array('update', 'id'=>$model->cambio_id)),
	array('label'=>'Delete EdmCambios', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->cambio_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EdmCambios', 'url'=>array('admin')),
);
?>

<h1>View EdmCambios #<?php echo $model->cambio_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'cambio_id',
		'cambio_documento',
		'cambio_nombre',
		'cambio_archivo',
		'cambio_fechayhora',
		'cambio_usuario',
		'cambio_privilegios',
		'cambio_descripcion',
		'cambio_activo',
	),
)); ?>
