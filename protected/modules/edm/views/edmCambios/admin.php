<?php
/* @var $this EdmCambiosController */
/* @var $model EdmCambios */

$this->breadcrumbs=array(
	'Edm Cambioses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List EdmCambios', 'url'=>array('index')),
	array('label'=>'Create EdmCambios', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#edm-cambios-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Edm Cambios</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'edm-cambios-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'cambio_id',
		'cambio_documento',
		'cambio_nombre',
		'cambio_archivo',
		'cambio_fechayhora',
		'cambio_usuario',
		'cambio_privilegios',
		'cambio_descripcion',
		'cambio_activo',

		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
