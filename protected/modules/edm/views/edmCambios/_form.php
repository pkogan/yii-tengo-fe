<?php
/* @var $this EdmCambiosController */
/* @var $model EdmCambios */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'edm-cambios-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'cambio_documento'); ?>
		<?php echo $form->textField($model,'cambio_documento'); ?>
		<?php echo $form->error($model,'cambio_documento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cambio_nombre'); ?>
		<?php echo $form->textField($model,'cambio_nombre',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'cambio_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cambio_archivo'); ?>
		<?php echo $form->textField($model,'cambio_archivo',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'cambio_archivo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cambio_fechayhora'); ?>
		<?php echo $form->textField($model,'cambio_fechayhora'); ?>
		<?php echo $form->error($model,'cambio_fechayhora'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cambio_usuario'); ?>
		<?php echo $form->textField($model,'cambio_usuario'); ?>
		<?php echo $form->error($model,'cambio_usuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cambio_privilegios'); ?>
		<?php echo $form->textField($model,'cambio_privilegios'); ?>
		<?php echo $form->error($model,'cambio_privilegios'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cambio_descripcion'); ?>
		<?php echo $form->textField($model,'cambio_descripcion',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'cambio_descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cambio_activo'); ?>
		<?php echo $form->textField($model,'cambio_activo'); ?>
		<?php echo $form->error($model,'cambio_activo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>
                
</div><!-- form -->