<?php
/* @var $this EdmDocumentoController */
/* @var $dataProvider CActiveDataProvider */
//exit(print_r($this));
 
$this->redirect(array('/edm/EdmDocumento/admin'));
$this->breadcrumbs=array(
	'Edm Documentos',
);

$this->menu=array(
	array('label'=>'Create EdmDocumento', 'url'=>array('create')),
	array('label'=>'Manage EdmDocumento', 'url'=>array('admin')),
);
?>

<h1>Edm Documentos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
