<?php
/* @var $this EdmDocumentoController */
/* @var $model EdmDocumento */

$this->breadcrumbs=array(
	'Edm Documentos'=>array('index'),
	$model->documento_id,
);

$this->menu=array(
	//array('label'=>'List EdmDocumento', 'url'=>array('index')),
	array('label'=>'Crear documento', 'url'=>array('create')),
	array('label'=>'Modificar este documento', 'url'=>array('update', 'id'=>$model->documento_id)),
	//array('label'=>'Delete EdmDocumento', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->documento_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Listar documentos', 'url'=>array('admin')),
);
?>

<h1>Documento #<?php echo $model->documento_id; ?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'documento_id',
		'documento_nombre',
		'documento_archivo',
		'documento_fechayhora',
		'documento_usuario',
		'documento_privilegios',
		'documento_descripcion',
		'documento_activo',
		'documento_proyecto',
	),
)); ?>

<!-- ---------------------------------------------------- -->
<br/>

<script>
//MOSTRAR MODAL
function apareceModalCasero()
{
    var elementos = document.getElementsByClassName('modalCasero')[0]; 
    elementos.style.visibility = "visible";
    elementos.style.background = "rgba(1, 1, 1, 0.6)";
 
    var elementosInternos = document.getElementsByClassName('modalBodyCasero')[0]; 
    elementosInternos.style.visibility = "visible";
    elementosInternos.style.background = "rgba(255, 255, 255, 1)";
}

//--------------------------------------------------------------------------
// OCULTAR MODAL
function ocultarModalCasero()
{	
   var elementos = document.getElementsByClassName('modalCasero')[0];    
   elementos.style.visibility = "hidden";
   elementos.style.background = "rgba(200, 200, 200, 0.1)";
		     
   var elementosInternos = document.getElementsByClassName('modalBodyCasero')[0]; 
   elementosInternos.style.visibility = "hidden";	  
 }
 
</script>
 
<a href="#" onclick="apareceModalCasero()">Ver archivo</a>
<?php    
    echo CHtml::linkButton('Descargar Archivo',
       array(
            'submit'=>array('edmDocumento/descargarArchivo' , array('file'=> $model->documento_nombre.DIRECTORY_SEPARATOR.$model->documento_archivo)),
            'confirm' => ' ¿ Quiere descargar el archivo ?'         
        )            
    );    
?>

<br/>
<hr/>
<h2>Registro de actividad sobre este documento </h2>

<style type="text/css">    
.modalCasero /*la parte de atras */
 {
   visibility: hidden;
   height:100%;
   width:100%;
   position: fixed;
   top:0px;   
   left:0px;
   z-index: 3;
   background: rgba(1, 2, 1, 0.2);
   transition: background 2s, visibility 0.8s;
   -moz-transition: background 2s, visibility 0.8s; /* Firefox 4 */
   -webkit-transition: background 2s, visibility 0.8s; /* Chrome y Safari */
   -o-transition: background 2s, visibility 0.8s; /* Opera */ 
 }
    
 /* hace referencia al div que contiene el form de login y el form de registro */
 .modalBodyCasero /* lo que se ve*/
 {
    visibility: hidden;
    height:90%;
    width:90%;
    margin:2.5% auto; /* este margin corre desde el contenedor y nos desde el lateral */	
    box-shadow: 0px 0px 20px black;    
    /*border-radius: 20px;*/
    background: rgba(255, 255, 255, 0.0);
    z-index: 15;
    padding: 10px;    
    padding-left: 30px;
    padding-right:20px;
    overflow-y:scroll;
}

.modalHeader /* para el boton cerrar */
{
    position:absolute;
    top:1px;
    right:1px;  
    width:30px;
    height:30px;    
    margin-bottom:0px; 
}

.modalHeader a
{
    text-decoration: none;
    color:white;
    font-size:25px;
}

/* ---- hasta aca ----- */
.logDeCambios
{
     
     width:700px;
     font-size: 16px;
     padding:5px;
     font-weight: bold;
     
}
</style>

<div class="logDeCambios">
    <?php        
        if(is_array($cambios))
        {           
           echo "<table>"; 
           echo "<tr><th>Fecha</th><th>Hora</th><th> Registro del cambio</th></tr>";           
           foreach($cambios as $cambio){
               $fecha = substr($cambio->cambio_fechayhora, 0, 10); //echo "fecha: ".$fecha."<br/>";
               $hora  = substr($cambio->cambio_fechayhora, 10, strlen((string) $cambio->cambio_fechayhora)); //echo "hora: ".$hora."<br/>";
               echo "<tr><td>".$fecha."</td><td>".$hora."</td> <td>".$cambio->cambio_descripcion."</td></tr>";               
           }              
           echo "</table>";           
        }else{
            echo "(No se encontraron registros anteriores de este documento)";
        }      
    ?>      
</div>

<div class="modalCasero">
    <div class="modalHeader"><a href="#" title="cerrar" onclick="ocultarModalCasero()">X</a></div>
    <div class="modalBodyCasero">                
    <?php 
       $path  = Yii::app()->basePath.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'edm'.DIRECTORY_SEPARATOR.'archivos'.DIRECTORY_SEPARATOR;
       $path .= $model->documento_nombre;
       $path .= DIRECTORY_SEPARATOR;
       $path .= $model->documento_archivo;   

       if(is_file($path))
       {                                          
           $array = pathinfo($path);           
           $ext = strtolower($array['extension']); 
           
           /* ----------------------------- */
           if($ext == 'txt')//si es un archivo de texto puedo tranquilamente visualizarlo
           {                        
               try{
                   $archivo = utf8_encode(file_get_contents($path));
                   $arreglo = explode("\n", $archivo);   
                   $i = 0;
                   while($i<count($arreglo)){
                     echo $arreglo[$i];
                     echo "<br/>";
                     $i++;
                   }
               }catch(CHttpException $error){
                   Yii::log("No pudo abrir el archivo de texto y separarlo por sus saltos en linea".$error);
                   utf8_decode(readfile($path)); 
               }
           }   
                      
           /* ----------------------------- */                             
           if($ext == 'pdf') //abrir con el lector de pdf del navegador
           {             
              //echo "Ruta completa hacia el archivo a visualizar: ".$path;
              echo "<a href='".$path."' target='_blank'> Abrir archivo en nueva pestaña</a>";
              ?><script>widnow.open('<?php echo $path ?>');</script><?php
           }
           /* ----------------------------- */    
                      
           if($ext == 'png' || $ext == 'jpg' || $ext == 'gif') //si es una imagen o un gif muestro la imagen/gif
           {               
                echo "<img src='$path' alt='No se pudo visualizar la imagen, por favor, pruebe descargandola' />";
           }
           /* ----------------------------- */
                      
           if($ext == 'doc')
           {               
               echo "No contamos con un visualizar de este tipo de archivo, sepa disculpar las molestias (aun puede descargarlo para visualizarlo)";
           }                    
           /* ----------------------------- */               
        }                
        ?>
    </div>
</div>

<?php  //echo $this->renderPartial('/edmCambios/view', array('cambios'=>$cambios)); ?>  