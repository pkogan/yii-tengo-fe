<?php
/* @var $this EdmDocumentoController */
/* @var $model EdmDocumento */

$this->breadcrumbs=array(
	'Edm Documentos'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List EdmDocumento', 'url'=>array('index')),
	array('label'=>'Crear un nuevo Documento', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#edm-documento-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h1>Documentos</h1>
<style>
    
tr.top td { border-top:1px solid gray; }

</style>
  
<script>    
    function abrirDir(dirname)
    {        
          var dir = document.getElementsByClassName(dirname);
          var i = 0;
          if(dir[0].style.display == 'none')
          {
             while(i<dir.length)
             {
                  dir[i].style.display = 'table-row';
                  i++;
             }
          }
          else
          {
              while(i<dir.length)
              {
                  dir[i].style.display = 'none';
                  i++;
              }
          }               
    }
</script>

<?php
/*
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p> 
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
*/
?>

<?php 
         //PARA VISUALIZAR LOS ARCHIVOS      
         $ruta = Yii::app()->basePath.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'edm'.DIRECTORY_SEPARATOR.'archivos'.DIRECTORY_SEPARATOR;                              
         //echo "Ruta del documento: <b> ".$ruta."</b>";         
         echo "<br/>";
         echo "<hr/>";     
                  
         //echo dirname($ruta);
         //echo "<br/>";
         //echo $ruta.DIRECTORY_SEPARATOR.Yii::getPathOfalias('webroot'); 
  
         //Abrir un directorio y listarlo recursivo          
         if(is_dir($ruta))
         {                
           if($dh = opendir($ruta))
           {                
              echo "<table border='1'>";
              echo "<tr height='30px'><th>Proyecto</th>  
                                <th>Archivo</th><th>Abrir archivo</th>
                                                            <th>Descarga</th><th>Tamaño</th></tr>";
                            
              $docsOfProject = array();                           
              foreach($model as $modelo){                  
                  $docsOfProject[$modelo->documento_nombre] = $modelo->attributes;                                    
              }
                            
              
             // echo "<pre>";print_r($docsOfProject);echo "</pre>"; die;                                                     
              foreach(scandir($ruta) as $posicion => $file)
              {              
                 if($file != '.' && $file != '..')                              
                 {                    
                     $esDir = false;                           
                     if(is_dir($ruta.$file) && !empty($docsOfProject[$file])){
                               $esDir = true;                                   
                               echo "<tr class=\"top bottom row\" >";
                               echo "<td width='100px'> proyecto x </td>";    
                               echo "<td><a href='#this' onclick='abrirDir(\"$file\")' style='text-decoration:none;'><img src='http://cdn1.iconfinder.com/data/icons/gloss-basic-icons-by-momentum/32/folder_closed.png' width='17' height='17' alt='+'  /> ".$file."</a></td>";                                                                
                               echo "<td></td>";
                               echo "<td></td>";
                               echo "<td></td>";
                               echo "</tr>";  
                        }                          
                 if($esDir) //para abrir la subcarpeta
                 {                                 
                      foreach(scandir($ruta.$file) as $subPosicion => $subFile)                                         
                      {
                           if($subFile != '.' && $subFile != '..')
                           {                        
                                echo "<tr class='$file' style='display:none;' > 
                                      <td> </td>                                
                                      <td> &nbsp;&nbsp;&nbsp;&nbsp;<i><a href='#this' style='text-decoration:none;'><img src='http://dl.openhandhelds.org/dingoo/screenshots/txt_icon.gif' width='17' height='17' alt='+' />".$subFile."</a></i></td>
                                      <td>".                                       
                                      CHtml::link(' Ver archivo ',
                                                  array('edmDocumento/view',
                                                        'id'=> $docsOfProject[$file]['documento_id'],                                                      
                                                   ))                                                                       
                                      ."</td>                                                                                 
                                      <td>".                                 
                                      CHtml::linkButton('Descargar Archivo',
                                         array(                                                                                          
                                               'submit'=>array('edmDocumento/descargarArchivo' , array('file'=> $docsOfProject[$file].DIRECTORY_SEPARATOR.$file.DIRECTORY_SEPARATOR.$docsOfProject[$file]['documento_archivo'])),
                                               'confirm' => ' ¿ Quiere descargar el archivo ?'         
                                              )            
                                        )      //<a href='".Yii::app()->basePath.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'edm'.DIRECTORY_SEPARATOR.'archivos'.DIRECTORY_SEPARATOR."'>Ver archivo</a>                                        
                                      ."</td>                                                                        
                                      <td>".filesize($ruta.$file.DIRECTORY_SEPARATOR.$subFile)." bytes</td>
                                      </tr>";                                                 
                            }                            
                       }            
                      $esDir=false;                                     
                  }                                                  
                }                                          
              }//fin del foreach              
                 closedir($dh);                  
                 echo "</table>";
            } 
          }          
          else{ 
                echo "<br>No es ruta valida";
          }                                         
/*         
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'edm-documento-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                //'documento_id',
		'documento_nombre',
                'documento_archivo',
		'documento_fechayhora',
		'documento_usuario',
		'documento_privilegios',
		'documento_descripcion',
		'documento_activo',
		'documento_proyecto',	
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{view} {update}'
		),
           ),
)); */ ?> 