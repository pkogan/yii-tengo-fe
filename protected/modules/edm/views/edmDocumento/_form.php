<?php
/* @var $this EdmDocumentoController */
/* @var $model EdmDocumento */
/* @var $form CActiveForm */
?>

<div class="form">    
<?php 
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'edm-documento-form',
	'enableAjaxValidation'=>true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
        
    <?php //echo $form->errorSummary($model); ?>
        
    <table border="1">
        
        <?php
        if($model->isNewRecord)
        {        
        ?>
            <tr>
                <td><?php echo $form->labelEx($model,'documento_nombre'); ?></td>
                <td><?php echo $form->textField($model,'documento_nombre',array('size'=>60,'maxlength'=>255)); ?></td>
                <td><?php echo $form->error($model,'documento_nombre'); ?></td>
            </tr>        
        <?php
        }                
        ?>
        
        <tr>
	    <td><?php //echo $form->labelEx($model,'documento_archivo'); ?></td>
            <td><?php //echo $form->hiddenField($model,'documento_archivo',array('size'=>60,'maxlength'=>100)); ?></td>
	    <td><?php //echo $form->error($model,'documento_archivo'); ?></td>
        </tr>
        
        <!--
        <tr>
            <td><?php echo $form->labelEx($model,'documento_fechayhora'); ?></td>
	    <td><?php echo $form->textField($model,'documento_fechayhora'); ?></td>
	    <td><?php echo $form->error($model,'documento_fechayhora'); ?></td>
        </tr>
        -->
        <!--
        <tr>
	    <td><?php //echo $form->labelEx($model,'documento_usuario'); ?></td>
	    <td><?php //echo $form->textField($model,'documento_usuario'); ?></td>
	    <td><?php //echo $form->error($model,'documento_usuario'); ?></td>
        </tr>
        -->        
                
	<tr>
            <td><?php echo $form->labelEx($model,'documento_privilegios'); ?></td>
	    <td><?php echo $form->textField($model,'documento_privilegios'); ?></td>
	    <td><?php echo $form->error($model,'documento_privilegios'); ?></td>
        </tr>
        
	<tr>
	    <td><?php echo $form->labelEx($model,'documento_descripcion'); ?></td>
	    <td><?php echo $form->textArea($model,'documento_descripcion',array('size'=>180,'maxlength'=>500, 'cols'=> 50, 'rows'=> 5, 'style' => 'resize:none;')); ?></td>
	    <td><?php echo $form->error($model,'documento_descripcion'); ?></td>
        </tr>

        <!--
	<tr>
            <td><?php echo $form->labelEx($model,'documento_activo'); ?></td>
	    <td><?php echo $form->textField($model,'documento_activo'); ?></td>
	    <td><?php echo $form->error($model,'documento_activo'); ?></td>
        </tr>
        -->
         
        <?php 
        if($model->isNewRecord)
        {
        ?>
            <tr>
                <td><?php echo $form->labelEx($model,'documento_proyecto'); ?></td>
                <td><?php echo $form->dropDownList($model,'documento_proyecto', CHtml::listData(Proyecto::model()->findAll(array('order'=>'id')), 'id', 'Proyecto')); ?></td>
                <td><?php echo $form->error($model,'documento_proyecto'); ?></td>
            </tr>
        <?php 
        } 
        ?>        
        <!--
	<tr>
            <td><?php // echo $form->labelEx($model,'documento_proyecto'); ?></td>
            <td><?php //echo $form->textField($model,'documento_proyecto'); ?></td>
	    <td><?php //echo $form->error($model,'documento_proyecto'); ?></td>
        </tr>
        -->        
    </table>
        
    <!-- seleccionar el archivo -->
    <?php echo CHtml::activeFileField($model, 'file'); ?>
    <!-- ---------------------- -->

        <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->