<?php
/* @var $this EdmDocumentoController */
/* @var $model EdmDocumento */

$this->breadcrumbs=array(
	'Edm Documentos'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List EdmDocumento', 'url'=>array('index')),
	array('label'=>'Listar Documentos', 'url'=>array('admin')),
);
?>


<h1>Crear EdmDocumento</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>