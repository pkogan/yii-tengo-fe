<?php
/* @var $this EdmDocumentoController */
/* @var $data EdmDocumento */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('documento_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->documento_id), array('view', 'id'=>$data->documento_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('documento_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->documento_nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('documento_archivo')); ?>:</b>
	<?php echo CHtml::encode($data->documento_archivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('documento_fechayhora')); ?>:</b>
	<?php echo CHtml::encode($data->documento_fechayhora); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('documento_usuario')); ?>:</b>
	<?php echo CHtml::encode($data->documentoUsuario->NombreApellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('documento_privilegios')); ?>:</b>
	<?php echo CHtml::encode($data->documento_privilegios); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('documento_descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->documento_descripcion); ?>
	<br />
        
	<?php  echo CHtml::encode($data->getAttributeLabel('documento_activo')); ?>
	<?php  echo CHtml::encode($data->documento_activo); ?>

	<b><?php echo CHtml::encode($data->getAttributeLabel('documento_proyecto')); ?>:</b>
	<?php echo CHtml::encode($data->documento_proyecto); ?>
	<br />

	?>
        
</div>