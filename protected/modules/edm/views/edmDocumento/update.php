<?php
/* @var $this EdmDocumentoController */
/* @var $model EdmDocumento */

$this->breadcrumbs=array(
	'Edm Documentos'=>array('index'),
	$model->documento_id=>array('view','id'=>$model->documento_id),
	'Update',
);

$this->menu=array(
	//array('label'=>'List EdmDocumento', 'url'=>array('index')),
	array('label'=>'Crear un nuevo Documento', 'url'=>array('create')),
	array('label'=>'Ver este documento', 'url'=>array('view', 'id'=>$model->documento_id)),
	array('label'=>'Listar documentos', 'url'=>array('admin')),
);
?>

<h1>Update EdmDocumento <?php echo $model->documento_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>