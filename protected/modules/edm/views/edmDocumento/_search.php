<?php
/* @var $this EdmDocumentoController */
/* @var $model EdmDocumento */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
    
       <div class="row">
		<?php echo $form->label($model,'documento_id'); ?>
		<?php echo $form->textField($model,'documento_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'documento_nombre'); ?>
		<?php echo $form->textField($model,'documento_nombre',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'documento_archivo'); ?>
		<?php echo $form->textField($model,'documento_archivo',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'documento_fechayhora'); ?>
		<?php echo $form->textField($model,'documento_fechayhora'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'documento_usuario'); ?>
		<?php echo $form->textField($model,'documento_usuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'documento_privilegios'); ?>
		<?php echo $form->textField($model,'documento_privilegios'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'documento_descripcion'); ?>
		<?php echo $form->textField($model,'documento_descripcion',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'documento_activo'); ?>
		<?php echo $form->textField($model,'documento_activo'); ?>
	</div>
    
	<div class="row">
		<?php echo $form->label($model,'documento_proyecto'); ?>
		<?php echo $form->textField($model,'documento_proyecto'); ?>
	</div>
    
	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->