<?php

class EdmDocumentoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'create', 'update', 'admin', 'descargarArchivo'),
				'roles'=>array('administrador'),
			),
			array('deny', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('*'),
				'users'=>array('*'),
			),
		
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */        
	public function actionView($id)
	{            
            $model = $this->loadModel($id);
            //$cambios =  EdmCambios::model()->findByPk($id);      
            $criteria = new CDbCriteria;             
            $criteria->condition='cambio_documento=:cambio_documento';
            $criteria->params=array(':cambio_documento'=>$id);                        
            $cantidad = EdmCambios::model()->count($criteria);                 
            //$cambios = EdmCambios::model()->findAll($criteria); //devuelve un array, q no lo puedo usar en el view 
            $cambios = EdmCambios::model()->findAllByAttributes(array('cambio_documento'=>$id)); //devuelve un objeto q es lo q se necesita
                
            
            //$cambios = (object) $cambios;                                                            
            $this->render('view',array(
			'model'=>$model,
                        'cambios'=> $cambios,
                        'cantidad' => $cantidad
            ));                                     
	}
             
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new EdmDocumento;
		//Uncomment the following line if AJAX validation is needed
		//$this->performAjaxValidation($model);
                                
		if(isset($_POST['EdmDocumento']))
		{                    
                   $file = CUploadedFile::getInstance($model,'file');                                                                            
                   $model->attributes=$_POST['EdmDocumento'];                   
                          
                   if(is_object($file))      //&& get_class($file) == 'CUploadedFile'
                   {                     
                     $path = Yii::app()->basePath.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'edm'.DIRECTORY_SEPARATOR.'archivos'.DIRECTORY_SEPARATOR.$model->documento_nombre;                        
                     if(!is_dir($path))
                                mkdir($path);
                                         
                     /* --------------------------------- */                                      
                     $url   = $path.DIRECTORY_SEPARATOR.$file->name; //url definitiva al archivo                                                            
                     $array = pathinfo($url);                     
                     $ext   = $array['extension'];
                     $ext   = strtolower($ext);                     
                     /* --------------------------------- */                                      
                     
                     if(empty($ext) OR $ext != 'txt' && $ext != 'pdf' &&  $ext != 'png' && $ext != 'jpg' && $ext != 'gif'){                         
                          throw new CHttpException(204, " Solo puede subir archivos con extension (.txt, .pdf, .png, .jpg, .gif");                          
                     }
                     else{                         
                         $file->saveAs($url);  
                     }     
                                          
                     /* --------------------------------------- */                                          
                     $model->documento_archivo    = $file->name;                        
                     $model->documento_usuario    = Yii::app()->user->getState('idUsuario');
                     $model->documento_activo     = 1;  //siempre se pone como activo
                     $model->documento_fechayhora = date('Y-m-d H:i:s');                     
                     /* --------------------------------------- */                                           
                     if($model->save())
                        $this->redirect(array('view','id'=>$model->documento_id));                    
                     else                         
                        throw new CHttpException(204, " No se pudo hacer el save File:".__FILE__." linea: ".__LINE__);
                     /* --------------------------------------- */                     
                   }else{
                        throw new CHttpException(20, 'Suba un archivo por favor');
                   }                           
                }                                
		$this->render('create',array(
			'model'=>$model,
		));
	}
        
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */         
	public function actionUpdate($id)
	{            
	   $model=$this->loadModel($id);                                                
           //Uncomment the following line if AJAX validation is needed
	   //$this->performAjaxValidation($model);                                                               
	   if(!empty($_POST['EdmDocumento']))
	   {            
              $cambio = new EdmCambios;                       
              $cambio->cambio_documento   = $model->documento_id;  //referencia al documento actual
              $cambio->cambio_nombre      = $model->documento_nombre;
              $cambio->cambio_archivo     = $model->documento_archivo;
              $cambio->cambio_fechayhora  = $model->documento_fechayhora; // deberia ser automatica en la insercion a la database
              $cambio->cambio_usuario     = $model->documento_usuario; 
              $cambio->cambio_privilegios = $model->documento_privilegios; //deberian ser los mismos ? creo q si
              $cambio->cambio_descripcion = $model->documento_descripcion;
              $cambio->cambio_activo      = $model->documento_activo;
                            
              if($cambio->save()) //si se guarda el actual documento en el cambio
              {                  
                  $model->attributes = $_POST['EdmDocumento'];
                                    
                  /* ---- Para el archivo y su ruta  ---*/
                  $file = CUploadedFile::getInstance($model,'file');
                  if(is_object($file)) //significa que recupero el archivo subido                      
                  {                        
                    /* ----------------------------------- */                                              
                    $path = Yii::app()->basePath.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'edm'.DIRECTORY_SEPARATOR.'archivos'.DIRECTORY_SEPARATOR.$model->documento_nombre;                     
                    if(!is_dir($path))
                            mkdir($path);
                                                                                  
                    $url = $path.DIRECTORY_SEPARATOR.$file->name; //url definitiva al archivo
                    /* ----------------------------------- */                                                 
                    $array = pathinfo($url);                     
                    $ext   = $array['extension'];
                    $ext   = strtolower($ext);
                                          
                    if(empty($ext) OR $ext != 'txt' && $ext != 'pdf' &&  $ext != 'png' && $ext != 'jpg' && $ext != 'gif'){                         
                         throw new CHttpException(204, " Solo puede subir archivos con extension (.txt, .pdf, .png, .jpg, .gif");                          
                    }
                    else{                                            
                        $file->saveAs($url);  
                        $guardoArchivo = true;
                    }                                     
                    /* ----------------------------------- */                                                                                              
                    if(!empty($guardoArchivo))
                    {                    
                        $model->documento_archivo = $file->name;                                                               
                        if($model->save())
                            $this->redirect(array('view','id'=>$model->documento_id)); 
                        else
                            throw new CHttpException(102, "Pudo guardar el archivo pero no la referencia en la BBDD");
                    }
                    else
                       throw new CHttpException(204, "Hubo un problema al cargar guardar el archivo");                                                               
                  }                      
               }           
	     }
                                       
             $this->render('update',array(
                    'model'=>$model,
             ));
	}
                
        protected function devuelveRuta($file)
        {
            
        }
        
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
        
        /*public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}               
	/**
	 * Lists all models.
	 */
        
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('EdmDocumento');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{         
              
            /*$model=new EdmDocumento('search');
	      $model->unsetAttributes();  // clear any default values
	      if(isset($_GET['EdmDocumento']))
                  $model->attributes=$_GET['EdmDocumento']; */ 
            $proyecto = Proyecto::model()->find('id=:id', array(':id'=> $id));
           if (!Yii::app()->user->checkAccess('usuarioVinculadoProyecto',
                               array('Proyecto' => $proyecto))){
               throw new CHttpException(404 , ' Usted no esta vinculado al proyecto, no tiene permisos ');                
               
           }
          
            $model = EdmDocumento::model()->findAll('documento_proyecto=:documento_proyecto', array('documento_proyecto'=>$id));
                     
            /*
            if (!Yii::app()->user->checkAccess('usuarioVinculadoProyecto', array('Proyecto' => $model)))
                       throw new CHttpException('404','El usuario no esta vinculado al Proyecto');
            */
            
            //if(empty($model))                     throw new CHttpException(404, "No se encontraron documentos para el proyecto ");
                //throw new CHttpException('404', 'No se encontraron documentos para el proyecto ');
           
            
	      $this->render('admin',array(
			'model'=>$model,
	    ));              
	}
        
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return EdmDocumento the loaded model
	 * @throws CHttpException
	 */
        public function loadModel($id)
	{               
           $model=EdmDocumento::model()->findByPk($id);
	   //$cambios = EdmCambios::model()->find($id);
           /* @var $model EdmDocumento */
           /* ----------------- SEGURIDAD ------------------- */
           $proyecto = $model->documentoProyecto;//Proyecto::model()->find('id=:id', array(':id'=> $model->documento_proyecto));
           if (!Yii::app()->user->checkAccess('usuarioVinculadoProyecto',
                               array('Proyecto' => $proyecto, 'Funcion' => Proyecto::$CLIENTE))){
                            
               throw new CHttpException(404 , ' Usted no esta vinculado al proyecto, no tiene permisos ');                
           }
           /* ---------------------------------------------- */           
           
           if($model===null)
	       throw new CHttpException(404,'The requested page does not exist.');
	           
           return $model;
	}
        
	/**
	 * Performs the AJAX validation.
	 * @param EdmDocumento $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='edm-documento-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}        
                
        public function actionDescargarArchivo()
        {                             
            if(!empty($_REQUEST['0']))
            {   
                $request = $_REQUEST['0'];
                $path = Yii::app()->basePath.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'edm'.DIRECTORY_SEPARATOR.'archivos'.DIRECTORY_SEPARATOR;                     
                $file = $path.$request['file'];                    
                              
                /* ---------------------------- */
                header('Content-disposition: attachment; filename='.$file);                   
                header("Content-Type: application/force-download");                
                /* ---------------------------- */
                $dirInfo = pathinfo($file);                
                $nombre = $dirInfo['basename'];                                            
                $ext = $dirInfo['extension'];                
                /* ----------------------------- */                
                if(strtolower($ext) == 'pdf')
                    header('Content-type: application/pdf');                                                
                            
                header("Content-disposition: attachment; filename=".$nombre);
                /* ------------------------------ */                
                $file = file_get_contents($file);
                echo $file;
                die;                  
            }else{
                throw new CHtppException(204, " No se ah encontrado el archivo solicitado ");
            }                      
        }                
}