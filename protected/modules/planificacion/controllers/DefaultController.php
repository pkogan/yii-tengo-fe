<?php

class DefaultController extends Controller
{
	public $layout='//layouts/column2';
	public function actionIndex($id)
	{
		$model=$this->loadModel($id);
		
		if(!Yii::app()->user->checkAccess('usuarioVinculadoProyecto',array('Proyecto'=>$model))){
			throw new CHttpException('El usuario no esta vinculado al Proyecto');
		}
		
		
		$dataproviderEtapa= new Etapaproyecto('search');
		$dataproviderEtapa->idProyecto=$id;
		$dataproviderEtapa=$dataproviderEtapa->search();
		$dataproviderEtapa->pagination=false;
		
		$this->render('view',array(
				'model'=>$model,
				'dataproviderEtapa'=>$dataproviderEtapa
		));
	}
	
	public function loadModel($id)
	{
		$model=Proyecto::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}