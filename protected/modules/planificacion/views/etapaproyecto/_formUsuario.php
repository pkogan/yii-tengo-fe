<?php
/* @var $this ProyectoController */
/* @var $model Recursoproyecto */
/* @var $form CActiveForm */
/* @var $proyecto Proyecto */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'UsuarioProyectoFuncionj-form',
        'enableAjaxValidation' => false,
            ));
    ?>

    <p class="note">Nuevo Usuario</p>

        <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'idUsuario.Usuario'); ?>
		<?php echo $form->dropDownList($model, 'idUsuario', CHtml::listData($proyecto->usuarioProyectoFuncions, 'idUsuario', 'idUsuario0.NombreApellido'), array('empty' => 'Seleccionar..')); ?>
        <?php echo $form->error($model, 'idUsuario'); ?>
    </div>

    <div class="row buttons">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->