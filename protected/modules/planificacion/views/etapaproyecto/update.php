<?php
/* @var $this EtapaproyectoController */
/* @var $model Etapaproyecto */

$this->breadcrumbs=array(
	'Etapaproyectos'=>array('index'),
	$model->idEtapa=>array('view','id'=>$model->idEtapa),
	'Update',
);

$this->menu=array(
	array('label'=>'View Etapaproyecto', 'url'=>array('view', 'id'=>$model->idEtapa)),
);
?>

<h1>Update Etapaproyecto <?php echo $model->idEtapa; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>