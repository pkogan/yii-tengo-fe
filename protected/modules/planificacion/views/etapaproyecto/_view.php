<?php
/* @var $this EtapaproyectoController */
/* @var $data Etapaproyecto */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idEtapa')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idEtapa), array('view', 'id'=>$data->idEtapa)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idProyecto')); ?>:</b>
	<?php echo CHtml::encode($data->idProyecto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaInicio')); ?>:</b>
	<?php echo CHtml::encode($data->fechaInicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaFin')); ?>:</b>
	<?php echo CHtml::encode($data->fechaFin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('porcentaje')); ?>:</b>
	<?php echo CHtml::encode($data->porcentaje); ?>
	<br />


</div>