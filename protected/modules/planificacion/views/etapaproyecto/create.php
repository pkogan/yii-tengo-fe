<?php
/* @var $this EtapaproyectoController */
/* @var $model Etapaproyecto */

$this->breadcrumbs=array(
	'Etapaproyectos'=>array('index&idProyecto='.$_GET['id']),
	'Create',
);

$this->menu=array(
	array('label'=>'List Etapaproyecto', 'url'=>array('index')),
	array('label'=>'Manage Etapaproyecto', 'url'=>array('admin')),
);
?>

<h1>Create Etapaproyecto</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>