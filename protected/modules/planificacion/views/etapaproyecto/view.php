<?php
/* @var $this EtapaproyectoController */
/* @var $model Etapaproyecto */
/* @var $modelUsuario Recursoproyecto */

$this->breadcrumbs=array(
		'Proyecto'=>array('/planificacion/default/index&id='.$model->idProyecto),
		$model->idEtapa,
);

$this->menu=array(
		array('label'=>'List Etapaproyecto', 'url'=>array('index'),'visible'=>Yii::app()->user->checkAccess('usuarioVinculadoProyecto',array('Proyecto'=>$model->idProyecto0,'Funcion'=>  Proyecto::$LIDERPROYECTO))),
		array('label'=>'Create Etapaproyecto', 'url'=>array('create','id'=>$model->idProyecto),'visible'=>Yii::app()->user->checkAccess('usuarioVinculadoProyecto',array('Proyecto'=>$model->idProyecto0,'Funcion'=>  Proyecto::$LIDERPROYECTO))),
		array('label'=>'Update Etapaproyecto', 'url'=>array('update', 'id'=>$model->idEtapa),'visible'=>Yii::app()->user->checkAccess('usuarioVinculadoProyecto',array('Proyecto'=>$model->idProyecto0,'Funcion'=>  Proyecto::$LIDERPROYECTO))),
		array('label'=>'Delete Etapaproyecto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idEtapa),'confirm'=>'Are you sure you want to delete this item?'),'visible'=>Yii::app()->user->checkAccess('usuarioVinculadoProyecto',array('Proyecto'=>$model->idProyecto0,'Funcion'=>  Proyecto::$LIDERPROYECTO))),
		array('label'=>'Manage Etapaproyecto', 'url'=>array('admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.usuarios-button').click(function(){
	$('.usuarios-form').toggle();
	return false;
});

");
?>

<h1>
	Etapa del Proyecto #
	<?php echo $model->idEtapa; ?>
</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
		'idEtapa',
		'idProyecto',
		'nombre',
		'descripcion',
		'fechaInicio',
		'fechaFin',
		'porcentaje',
	),
));?>
<br/><h2>Usuarios Asignados</h2>
<?php if (Yii::app()->user->hasFlash('usuarioProyecto')): ?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('usuarioProyecto'); ?>
    </div>
<?php endif; ?>
<?php echo CHtml::link('Agregar Nuevo Usuario', '#', array('class' => 'usuarios-button')); ?>

<div class="usuarios-form search-form" style="<?php if (!$modelUsuario->hasErrors()) echo 'display:none'; ?>">
    <?php $this->renderPartial('_formUsuario', array('model' => $modelUsuario,'proyecto'=>$model->idProyecto0)) ?>
</div>
<br/><br/><h2>Usuarios asociados:</h2>


<?php 
$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'proyecto-grid',
		'dataProvider' => $dataproviderUsuario,
		'columns' => array(
        		'idUsuario0.NombreApellido',
				'idUsuario0.Email',	
		array(
		'class' => 'CButtonColumn',
		'template' => '{delete}',
		'buttons' => array(
		'delete' => array(
				'url' => 'Yii::app()->controller->createUrl(\'deleteUsuario\', array(\'idEtapa\'=>$data["idEtapa"],\'idUsuario\'=>$data["idUsuario"]))',
						),
				),
		),
	))
);
?>
