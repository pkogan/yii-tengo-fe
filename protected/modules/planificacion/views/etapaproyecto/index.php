<?php
/* @var $this EtapaproyectoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Etapaproyectos',
);

$this->menu=array(
	array('label'=>'Create Etapaproyecto', 'url'=>array('create')),
	array('label'=>'Manage Etapaproyecto', 'url'=>array('admin')),
);
?>

<h1>Etapaproyectos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider->search(),
	'itemView'=>'_view',
)); ?>
