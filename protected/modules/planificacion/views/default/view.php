<?php
/* @var $this ProyectoController */
/* @var $model Proyecto */
/* @var $etapa EtapaProyecto*/

$this->breadcrumbs=array(
		'Proyecto'=>array('/proyecto/index'),
		$model->id,
);

$this->menu=array(
		array('label'=>'List Proyecto', 'url'=>array('index'),'visible'=>Yii::app()->user->checkAccess('usuarioVinculadoProyecto',array('Proyecto'=>$model,'Funcion'=>  Proyecto::$LIDERPROYECTO))),
		//array('label'=>'Create Proyecto', 'url'=>array('create'),'visible'=>Yii::app()->user->checkAccess('administrador')),
		array('label'=>'Create Etapa', 'url'=>array('/planificacion/etapaproyecto/create', 'id'=>$model->id),'visible'=>Yii::app()->user->checkAccess('usuarioVinculadoProyecto',array('Proyecto'=>$model,'Funcion'=>  Proyecto::$LIDERPROYECTO))),
		//array('label'=>'Delete Proyecto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=>Yii::app()->user->checkAccess('administrador')),
		//array('label'=>'Manage Proyecto', 'url'=>array('admin'),'visible'=>Yii::app()->user->checkAccess('administrador')),
		array('label'=>'Requerimientos', 'url'=>array('/requerimientos/default/index', 'id'=>$model->id)),
		array('label'=>'Edm', 'url'=>array('/edm/default/index', 'id'=>$model->id)),
		array('label'=>'Wiki', 'url'=>array('/wiki/default/index', 'id'=>$model->id)),
		array('label'=>'Planificacion', 'url'=>array('/planificacion/default/index', 'id'=>$model->id)),
		array('label'=>'RRHH', 'url'=>array('/rrhh/default/index', 'id'=>$model->id)),
		array('label'=>'Metricas', 'url'=>array('/metricas/default/index', 'id'=>$model->id)),
		array('label'=>'Casos de Test', 'url'=>array('/test/default/index', 'id'=>$model->id)),
		array('label'=>'Incidentes', 'url'=>array('/incidentes/default/index', 'id'=>$model->id)),
);
?>

<h1>
	View Proyecto #
	<?php echo $model->id; ?>
</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
		'id',
		'Proyecto',
		'Descripcion',
		'FechaIncicio',
		'FechaFin',
		'idEstado0.EstadoProyecto',
//		'ValorHora',
//		'HorasSemanales',
//		'TiempoAjustadoPuntos',
	),
));

$total = 0;
$cont = 0;
foreach ($model->etapaproyectos as $etapa) {
	$total = $total + $etapa->porcentaje;
	$cont = $cont + 1;
}

if($cont!=0){
$total = $total/$cont;
echo '<br/><h3>Porcentaje de avance: </h3>';
echo '<b>'.$total.'</b> %<br/>';
}
?>
<?php
echo '<br/><h3>Etapas del proyecto:</h3>';
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'proyecto-grid',
    'dataProvider' => $dataproviderEtapa,
    'columns' => array(
		'nombre',
		'porcentaje',		
		array(
		'name'=>'vermas',
		'type'=>'raw',
		'header'=>'Vinculo',
		'value'=>'CHtml::link("ver mas", Yii::app()->request->baseUrl."/index.php?r=planificacion/etapaproyecto/view&id=".$data->idEtapa)',
)
    ),
));
?>
