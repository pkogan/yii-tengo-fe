<?php
/* @var $this TipotestController */
/* @var $model Tipotest */

$this->breadcrumbs=array(
	'Tipotests'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tipotest', 'url'=>array('index')),
	array('label'=>'Manage Tipotest', 'url'=>array('admin')),
);
?>

<h1>Create Tipotest</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>