<?php
/* @var $this TipotestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tipotests',
);

$this->menu=array(
	array('label'=>'Create Tipotest', 'url'=>array('create')),
	array('label'=>'Manage Tipotest', 'url'=>array('admin')),
);
?>

<h1>Tipotests</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
