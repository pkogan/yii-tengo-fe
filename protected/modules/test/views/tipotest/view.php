<?php
/* @var $this TipotestController */
/* @var $model Tipotest */

$this->breadcrumbs=array(
	'Tipotests'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Tipotest', 'url'=>array('index')),
	array('label'=>'Create Tipotest', 'url'=>array('create')),
	array('label'=>'Update Tipotest', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Tipotest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tipotest', 'url'=>array('admin')),
);
?>

<h1>View Tipotest #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tipo',
	),
)); ?>
