<?php
/* @var $this TipotestController */
/* @var $model Tipotest */

$this->breadcrumbs=array(
	'Tipotests'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tipotest', 'url'=>array('index')),
	array('label'=>'Create Tipotest', 'url'=>array('create')),
	array('label'=>'View Tipotest', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Tipotest', 'url'=>array('admin')),
);
?>

<h1>Update Tipotest <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>