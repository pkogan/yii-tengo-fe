<?php
/* @var $this ProyectoController */
/* @var $model Proyecto */

$this->breadcrumbs=array(
	'Proyectos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Proyecto', 'url'=>array('index')),
	array('label'=>'Create Proyecto', 'url'=>array('create'),'visible'=>Yii::app()->user->checkAccess('administrador')),
	array('label'=>'Update Proyecto', 'url'=>array('update', 'id'=>$model->id),'visible'=>Yii::app()->user->checkAccess('usuarioVinculadoProyecto',array('Proyecto'=>$model,'Funcion'=>  Proyecto::$LIDERPROYECTO))),
	array('label'=>'Delete Proyecto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=>Yii::app()->user->checkAccess('administrador')),
	array('label'=>'Manage Proyecto', 'url'=>array('admin'),'visible'=>Yii::app()->user->checkAccess('administrador')),
        array('label'=>'Requerimientos', 'url'=>array('/requerimientos/default/index', 'id'=>$model->id)),
        array('label'=>'Edm', 'url'=>array('/edm/default/index', 'id'=>$model->id)),
        array('label'=>'Wiki', 'url'=>array('/wiki/default/index', 'id'=>$model->id)),
        array('label'=>'Planificación', 'url'=>array('/planificacion/default/index', 'id'=>$model->id)),
        array('label'=>'RRHH', 'url'=>array('/rrhh/default/index', 'id'=>$model->id)),
        array('label'=>'Métricas', 'url'=>array('/metricas/default/index', 'id'=>$model->id)),
        array('label'=>'Casos de Test', 'url'=>array('/test/default/index', 'id'=>$model->id)),
        array('label'=>'Incidentes', 'url'=>array('/incidentes/default/index', 'id'=>$model->id)),
);
?>

<h1>Vista Proyecto #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'Proyecto',
		'Descripcion',
		'FechaIncicio',
		'FechaFin',
		'idEstado0.EstadoProyecto',
//		'ValorHora',
//		'HorasSemanales',
//		'TiempoAjustadoPuntos',
	),
)); ?>
