<?php
/* @var $this CambioestadosController */
/* @var $model Cambioestados */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cambioestados-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_estado'); ?>
		<?php echo $form->textField($model,'id_estado'); ?>
		<?php echo $form->error($model,'id_estado'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'id_estado'); ?>
		<?php //echo $form->textField($model,'id_tipo'); ?>
                <?php echo $form->dropDownList($model,'id_estado',
                        CHtml::listData(Estados::model()->findAll(),'id','estado')
                ); ?>
		<?php echo $form->error($model,'id_estado'); ?>
                
                <?php //echo $form->dropDownList($model, 'tipo_id', CHtml::listData(GesTiposcli::model()->findAll(array('order'=>'tipo')), 'id_tipocli','tipo'), array('empty'=>'Seleccionar..')); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'id_test'); ?>
		<?php echo $form->textField($model,'id_test'); ?>
		<?php echo $form->error($model,'id_test'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_usuario'); ?>
		<?php echo $form->textField($model,'id_usuario'); ?>
		<?php echo $form->error($model,'id_usuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fechahora'); ?>
		<?php echo $form->textField($model,'fechahora'); ?>
		<?php echo $form->error($model,'fechahora'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->