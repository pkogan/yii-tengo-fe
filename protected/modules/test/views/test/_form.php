<?php
/* @var $this TestController */
/* @var $model Test */
/* @var $form CActiveForm */
/* @var $modelTipoTest Tipotest */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'test-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> Son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<h3>
		
		<?php if($model->isNewRecord)
                    echo "Está trabajando sobre el proyecto con ID = ".$id; ?>
	</h3>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'resultadoesperado'); ?>
		<?php echo $form->textField($model,'resultadoesperado',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'resultadoesperado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pasos'); ?>
		<?php echo $form->textField($model,'pasos',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'pasos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'prioridad'); ?>
		<?php echo $form->textField($model,'prioridad',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'prioridad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_tipo'); ?>
		<?php //echo $form->textField($model,'id_tipo'); ?>
                <?php echo $form->dropDownList($model,'id_tipo',
                        CHtml::listData(Tipotest::model()->findAll(),'id','tipo')
                ); ?>
		<?php echo $form->error($model,'id_tipo'); ?>
                
                <?php //echo $form->dropDownList($model, 'tipo_id', CHtml::listData(GesTiposcli::model()->findAll(array('order'=>'tipo')), 'id_tipocli','tipo'), array('empty'=>'Seleccionar..')); ?>
	</div>

	<div class="row">

		<?php echo $form->labelEx($model,'fechahora'); ?>
		<?php echo $form->textField($model,'fechahora'); ?>
		<?php echo $form->error($model,'fechahora'); ?>
	</div>

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
