<?php
/* @var $this TestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tests',
);

$this->menu=array(
	array('label'=>'Crear Test', 'url'=>array('create', 'id'=>$model->id)),
	array('label'=>'Manage Test', 'url'=>array('admin','id'=>$model->id)),
	
);
?>

<h1>Casos de Test</h1>


<h1>Ver Test  #
<?php echo $model->id; ?>
</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
		'id',
		'Proyecto',
		'Descripcion',
		'FechaIncicio',
		'FechaFin',
		'idEstado0.EstadoProyecto',
//		'ValorHora',
//		'HorasSemanales',
//		'TiempoAjustadoPuntos',
	),
));



$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'test-grid',
	'dataProvider'=>$model2->search(),
	//'filter'=>$model2,
	'columns'=>array(
		'id',
		'id_proyecto',
		'descripcion',
		'resultadoesperado',
		'pasos',
		'prioridad',
		/*
		'id_tipo',
		'fechahora',
		'id_usuario_registro',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
));

//  $this->widget('zii.widgets.CListView', array(
// 	'dataProvider'=>$dataProvider,
// 	'itemView'=>'_view',
// )); ?>
