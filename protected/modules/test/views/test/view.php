<?php
/* @var $this TestController */
/* @var $model Test */

$this->breadcrumbs=array(
	'Tests'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Test', 'url'=>array('index', 'id'=>$model->id_proyecto)),
	array('label'=>'Create Test', 'url'=>array('create')),
	array('label'=>'Update Test', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Test', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Test', 'url'=>array('admin', 'id'=>$model->id)),
        array('label'=>'Cambiar estado de Test', 'url'=>array('cambioestados/create', 'id'=>$model->id)),
);
?>
<?php 
    $modelProyecto=Proyecto::model()->findByPk($model->id_proyecto);
    //echo $model->id_proyecto;
    //echo $modelProyecto->id;
?>
<h1>
	Proyecto #
	<?php echo $model->id_proyecto; ?>
</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$modelProyecto,
		'attributes'=>array(
		'id',
		'Proyecto',
		'Descripcion',
		'FechaIncicio',
		'FechaFin',
		'idEstado0.EstadoProyecto',
//		'ValorHora',
//		'HorasSemanales',
//		'TiempoAjustadoPuntos',
	),
));
?>



<h1>View Test  #
<?php echo $model->id; ?>
</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_proyecto',
		'descripcion',
		'resultadoesperado',
		'pasos',
		'prioridad',
		'id_tipo',
		'fechahora',
		'id_usuario_registro',
	),
)); ?>


<h1>View Cambio De Estados  #
<?php echo $model->id; ?>
</h1>
<?php
        //$modelCambioEstados= Cambioestados::model()->find('id_test='.$model->id);
        
?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cambioestados-grid',
	'dataProvider'=>$modelCambioEstados->search(),
	//'filter'=>$modelCambioEstados,
	'columns'=>array(
		'id',
		'id_estado',
		'id_test',
		'id_usuario',
		'descripcion',
		'fechahora',
		array(
			'class'=>'CButtonColumn',
			'template' => '{view}',
            'buttons' => array(
                'view' => array(
                    'url' => 'Yii::app()->controller->createUrl(\'cambioestados/view\', array(\'id\'=>$data["id"]))',
                ),
            ),

		),
	),
)); ?>