<?php
/* @var $this CambioestadosController */
/* @var $model Cambioestados */

$this->breadcrumbs=array(
	'Estados'=>array('index', 'id'=>$model->id_test),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar estados', 'url'=>array('index', 'id'=>$model->id_test)),
	array('label'=>'Administrar estados', 'url'=>array('admin', 'id'=>$model->id_test)),
);
?>

<h1>Create Cambioestados</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>