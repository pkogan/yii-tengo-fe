<?php
/* @var $this CambioestadosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cambioestados',
);

$this->menu=array(
	array('label'=>'Crear estado', 'url'=>array('create', 'id'=>$id)),
	array('label'=>'Administrar estados', 'url'=>array('admin', 'id'=>$id)),
);
?>

<h1>Cambio De Estados</h1>
<?php print_r(Yii::app()->user->id); ?>
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
