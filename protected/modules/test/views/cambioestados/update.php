<?php
/* @var $this CambioestadosController */
/* @var $model Cambioestados */

$this->breadcrumbs=array(
	'Cambioestadoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Cambioestados', 'url'=>array('index')),
	array('label'=>'Create Cambioestados', 'url'=>array('create')),
	array('label'=>'View Cambioestados', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Cambioestados', 'url'=>array('admin')),
);
?>

<h1>Update Cambioestados <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>