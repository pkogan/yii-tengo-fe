<?php
/* @var $this CambioestadosController */
/* @var $model Cambioestados */

$this->breadcrumbs=array(
	'Cambioestadoses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Cambioestados', 'url'=>array('index', 'id'=>$model->id_test)),
	array('label'=>'Create Cambioestados', 'url'=>array('create', 'id'=>$model->id_test)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cambioestados-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Cambioestadoses</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cambioestados-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		'id',
		'id_estado',
		'id_test',
		'id_usuario',
		'descripcion',
		'fechahora',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
