-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 07-05-2013 a las 06:33:03
-- Versión del servidor: 5.5.27
-- Versión de PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `yiitengofe`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambioestados`
--

CREATE TABLE IF NOT EXISTS `cambioestados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_estado` int(11) NOT NULL,
  `id_test` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `fechahora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_estado` (`id_estado`,`id_test`,`id_usuario`),
  KEY `id_test` (`id_test`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Modulo Test' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `EstadoProyecto`
--

CREATE TABLE IF NOT EXISTS `EstadoProyecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `EstadoProyecto` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `EstadoProyecto`
--

INSERT INTO `EstadoProyecto` (`id`, `EstadoProyecto`) VALUES
(1, 'Iniciado'),
(2, 'En Desarrollo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE IF NOT EXISTS `estados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `estado`) VALUES
(1, 'No Ejecutado'),
(2, 'Ejecutado OK'),
(3, 'Ejecutado Falla'),
(4, 'Dependiente'),
(5, 'No aplica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Funciones`
--

CREATE TABLE IF NOT EXISTS `Funciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Funcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `Funciones`
--

INSERT INTO `Funciones` (`id`, `Funcion`) VALUES
(1, 'Líder de Proyecto'),
(2, 'Programador'),
(3, 'Tester'),
(4, 'Analista Funcional'),
(5, 'Cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Proyecto`
--

CREATE TABLE IF NOT EXISTS `Proyecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Proyecto` varchar(1000) NOT NULL,
  `Descripcion` text NOT NULL,
  `FechaIncicio` datetime NOT NULL,
  `FechaFin` datetime DEFAULT NULL,
  `idEstado` int(11) NOT NULL,
  `ValorHora` double NOT NULL,
  `HorasSemanales` int(11) NOT NULL,
  `TiempoAjustadoPuntos` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`idEstado`),
  KEY `Proyecto_ibfk_1` (`idEstado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `Proyecto`
--

INSERT INTO `Proyecto` (`id`, `Proyecto`, `Descripcion`, `FechaIncicio`, `FechaFin`, `idEstado`, `ValorHora`, `HorasSemanales`, `TiempoAjustadoPuntos`) VALUES
(2, 'Proyecto 1', 'ldldl', '2013-05-07 00:00:00', '0000-00-00 00:00:00', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Rol`
--

CREATE TABLE IF NOT EXISTS `Rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Rol` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `Rol`
--

INSERT INTO `Rol` (`id`, `Rol`) VALUES
(1, 'administrador'),
(2, 'Empleado'),
(3, 'Cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_proyecto` int(11) NOT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `resultadoesperado` varchar(250) DEFAULT NULL,
  `pasos` varchar(250) DEFAULT NULL,
  `prioridad` varchar(25) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `fechahora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_usuario_registro` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_proyecto` (`id_proyecto`,`id_tipo`,`id_usuario_registro`),
  KEY `id_tipo` (`id_tipo`),
  KEY `id_usuario_registro` (`id_usuario_registro`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Modulo Test' AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `test`
--

INSERT INTO `test` (`id`, `id_proyecto`, `descripcion`, `resultadoesperado`, `pasos`, `prioridad`, `id_tipo`, `fechahora`, `id_usuario_registro`) VALUES
(2, 2, 'aaaaaaaaaa', 'aaaaaaaaaaaa', 'aaaaaaaaaaaaaa', 'aaaaaaa', 1, '2013-05-07 03:00:00', 1),
(3, 2, 'bbbbbbbbbbbbbb', 'bbbbbbbbbbbbb', 'bbbbbbbbbbbbb', 'bbbbbbbbbbbbbb', 1, '2013-05-07 03:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipotest`
--

CREATE TABLE IF NOT EXISTS `tipotest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Modulo Test' AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tipotest`
--

INSERT INTO `tipotest` (`id`, `tipo`) VALUES
(1, 'Funcional'),
(2, 'No Funcional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Usuario`
--

CREATE TABLE IF NOT EXISTS `Usuario` (
  `Usuario` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Clave` varchar(32) NOT NULL,
  `idRol` int(11) NOT NULL,
  `NombreApellido` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Usuario` (`Usuario`),
  KEY `idRol` (`idRol`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `Usuario`
--

INSERT INTO `Usuario` (`Usuario`, `id`, `Clave`, `idRol`, `NombreApellido`, `Email`) VALUES
('admin', 1, 'admin', 1, 'Administra Administrador', 'admin@tormenta.com'),
('desarrollador', 2, 'desarrollador', 2, 'Emplea Empleado', 'empleado@tormenta.com'),
('cliente', 3, 'cliente', 3, 'Clie Cliente', 'cliente@empresacliente.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UsuarioProyectoFuncion`
--

CREATE TABLE IF NOT EXISTS `UsuarioProyectoFuncion` (
  `idUsuario` int(11) NOT NULL,
  `idProyecto` int(11) NOT NULL,
  `idFuncion` int(11) NOT NULL,
  PRIMARY KEY (`idUsuario`,`idProyecto`,`idFuncion`),
  KEY `idFuncion` (`idFuncion`),
  KEY `idProyecto` (`idProyecto`),
  KEY `idUsuario` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `UsuarioProyectoFuncion`
--

INSERT INTO `UsuarioProyectoFuncion` (`idUsuario`, `idProyecto`, `idFuncion`) VALUES
(2, 2, 1),
(3, 2, 5);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cambioestados`
--
ALTER TABLE `cambioestados`
  ADD CONSTRAINT `cambioestados_ibfk_1` FOREIGN KEY (`id_test`) REFERENCES `test` (`id`),
  ADD CONSTRAINT `cambioestados_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `Usuario` (`id`),
  ADD CONSTRAINT `cambioestados_ibfk_3` FOREIGN KEY (`id_estado`) REFERENCES `estados` (`id`);

--
-- Filtros para la tabla `Proyecto`
--
ALTER TABLE `Proyecto`
  ADD CONSTRAINT `Proyecto_ibfk_1` FOREIGN KEY (`idEstado`) REFERENCES `EstadoProyecto` (`id`);

--
-- Filtros para la tabla `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `test_ibfk_1` FOREIGN KEY (`id_proyecto`) REFERENCES `Proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `test_ibfk_2` FOREIGN KEY (`id_tipo`) REFERENCES `tipotest` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `test_ibfk_3` FOREIGN KEY (`id_usuario_registro`) REFERENCES `Usuario` (`id`);

--
-- Filtros para la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD CONSTRAINT `Usuario_ibfk_1` FOREIGN KEY (`idRol`) REFERENCES `Rol` (`id`);

--
-- Filtros para la tabla `UsuarioProyectoFuncion`
--
ALTER TABLE `UsuarioProyectoFuncion`
  ADD CONSTRAINT `UsuarioProyectoFuncion_ibfk_1` FOREIGN KEY (`idProyecto`) REFERENCES `Proyecto` (`id`),
  ADD CONSTRAINT `UsuarioProyectoFuncion_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`id`),
  ADD CONSTRAINT `UsuarioProyectoFuncion_ibfk_3` FOREIGN KEY (`idFuncion`) REFERENCES `Funciones` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
