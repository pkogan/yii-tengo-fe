<?php

class TestController extends Controller
{
	// /**
	//  * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	//  * using two-column layout. See 'protected/views/layouts/column2.php'.
	//  */
	//public $layout='//layouts/column2';

	// /**
	//  * @return array action filters
	//  */
	// public function filters()
	// {
	// 	return array(
	// 		'accessControl', // perform access control for CRUD operations
	// 		'postOnly + delete', // we only allow deletion via POST request
	// 	);
	// }

	// /**
	//  * Specifies the access control rules.
	//  * This method is used by the 'accessControl' filter.
	//  * @return array access control rules
	//  */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
               // $dataProviderCambioEstados=new CActiveDataProvider('Cambioestados');
                $modelCambioEstados=new Cambioestados('search');
		$modelCambioEstados->unsetAttributes();  // clear any default values
                $modelCambioEstados->id_test=$id;
		//if(isset($_GET['Cambioestados']))
		//	$modelCambioEstados->attributes=$_GET['id'];
                
		$this->render('view',array(
			'model'=>$this->loadModel($id),
                        'modelCambioEstados'=>$modelCambioEstados,
                        //'dataProviderCambioEstados'=>$dataProviderCambioEstados,
		));
            /*
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		)); */
	}

	// /**
	//  * Creates a new model.
	//  * If creation is successful, the browser will be redirected to the 'view' page.
	//  */
	public function actionCreate($id)
	{
		$model=new Test;
		//$model->id_proyecto= $id; //<----
		$modelTipoTest=new Tipotest;
		$modelTipoTest->findAll();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Test']))
		{
			
			$model->attributes=$_POST['Test'];
			$model->id_proyecto=$id;
                        $model->id_usuario_registro= Yii::app()->user->idUsuario;
                        if($model->save())
				$this->redirect(array('index','id'=>$id));
		}
		$model->fechahora=date("Y-m-d H:m:s");
		
		$this->render('create', array(
			'model'=>$model, 'modelTipoTest'=>$modelTipoTest ,'id'=>$id
		));
	}

	 /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Test']))
		{
			$model->attributes=$_POST['Test'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	 }

	// /**
	//  * Deletes a particular model.
	//  * If deletion is successful, the browser will be redirected to the 'admin' page.
	//  * @param integer $id the ID of the model to be deleted
	//  */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	// /**
	//  * Lists all models.
	//  */
	// public function actionIndex()
	// {
	// 	$dataProvider=new CActiveDataProvider('Test');
	// 	$this->render('index',array(
	// 		'dataProvider'=>$dataProvider,
	// 	));
	// }

	// /**
	//  * Manages all models.
	//  */
	public function actionAdmin($id)
	{
		$model=new Test('search');
		
		$model->unsetAttributes();
		$model->id_proyecto=$id;
		  // clear any default values
		if(isset($_GET['Test']))
			$model->attributes=$_GET['Test'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	// /**
	//  * Returns the data model based on the primary key given in the GET variable.
	//  * If the data model is not found, an HTTP exception will be raised.
	//  * @param integer $id the ID of the model to be loaded
	//  * @return Test the loaded model
	//  * @throws CHttpException
	//  */
	// public function loadModel($id)
	// {
	// 	$model=Test::model()->findByPk($id);
	// 	if($model===null)
	// 		throw new CHttpException(404,'The requested page does not exist.');
	// 	return $model;
	// }

	// /**
	//  * Performs the AJAX validation.
	//  * @param Test $model the model to be validated
	//  */
	// protected function performAjaxValidation($model)
	// {
	// 	if(isset($_POST['ajax']) && $_POST['ajax']==='test-form')
	// 	{
	// 		echo CActiveForm::validate($model);
	// 		Yii::app()->end();
	// 	}
	// }

	public $layout='//layouts/column2';
	
	public function actionIndex($id)
	{
		

		$model=$this->loadModelProyecto($id);

			$dataProvider=new CActiveDataProvider('Test',array('criteria'=>array('condition'=>'id_proyecto=:id',
				'params'=>array(':id'=>$id))));
	

		if(!Yii::app()->user->checkAccess('usuarioVinculadoProyecto',array('Proyecto'=>$model))){
			throw new CHttpException('El usuario no esta vinculado al Proyecto');
		}
		
		$model2=new Test('search');
		
		$model2->unsetAttributes();
		$model2->id_proyecto=$id;
		  // clear any default values
		if(isset($_GET['Test']))
			$model2->attributes=$_GET['Test'];




		$this->render('index',array(
				'model'=>$model,'dataProvider'=>$dataProvider, 'model2'=>$model2,
		));
	}
	
	public function loadModelProyecto($id)
	{
		$model=Proyecto::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModel($id)
	{
		$model=Test::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
