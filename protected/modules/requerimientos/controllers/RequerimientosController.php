<?php

class RequerimientosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','deleteUsuarioAsignado'),
				'users'=>array('cliente'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','update'),
				'users'=>array('desarrollador'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','view','create','update','admin','delete','nuevoUsuarioAsignado','deleteUsuarioAsignado'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'actions'=>array('deleteUsuarioAsignado'),
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		
		$model=$this->loadModel($id);
		
		/*$rowdata=$model->usuariosasignadoses;
		
		//exit;
		$dataprovider= new CArrayDataProvider($rowdata,array(
				'keyField'=>false,
				'pagination'=>array(
						'PageSize'=>10),));*/
		$dataproviderUsuarios= new Usuariosasignados('search');
		$dataproviderUsuarios->idRequerimiento=$id;
		$dataproviderUsuarios=$dataproviderUsuarios->search();
		$dataproviderUsuarios->pagination=false;
		
		$this->render('view', array(
				'model' => $model,
				
				'dataprovider' => $dataproviderUsuarios
		));
		
		/*$dataproviderUsuarios=Usuariosasignados::model()->search();
		$dataproviderUsuarios->pagination=false;
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'dataproviderUsuarios'=>$dataproviderUsuarios,
		));*/
	}
	/**
     * Creates a new comment.
     * This method attempts to create a new comment based on the user input.
     * If the comment is successfully created, the browser will be redirected
     * to show the created comment.
     * @param Requerimiento $Requerimiento
     * @return UsuarioProyectoFuncion 
     */
	public  function actionNuevoUsuarioAsignado($idRequerimiento,$idUsuario) {
	
		$model = new Usuariosasignados();
		
		$model->idRequerimiento=$idRequerimiento;		
		$model->idUsuario=$idUsuario;
		
		/*if (isset($_POST['Usuariosasignados'])) {
			$usuariosAsignados->attributes = $_POST['Usuariosasignados'];
			$usuariosAsignados->idRequerimiento = $requerimiento->idRequerimiento;			
			if ($usuariosAsignados->findByAttributes($usuariosAsignados->getAttributes())) {
				$$usuariosAsignados->addErrors(array('idRequerimiento' => 'El usuario ya existe con esa funcion en el Proyecto'));
			} else {
				 
				/*if ($usuariosAsignados->save()) {
					Yii::app()->user->setFlash('usuarioProyecto', 'El Usuario ha sido agregado');
					$this->refresh();
				}
			}
		}*/
		if ($model->findByAttributes($model->getAttributes())) {
				//$model->addErrors(array('idRequerimiento' => 'El usuario ya esta asignado a este requerimiento'));
				Yii::app()->user->setFlash('error', 'El Usuario ya esta asignado a este requerimiento');
				$this->redirect(array('view','id'=>$idRequerimiento));
			} else {
				 
				if ($model->save()) {
					Yii::app()->user->setFlash('success', 'El Usuario ha sido asignado');
					
					$this->redirect(array('view','id'=>$idRequerimiento));
				}
			}
		
		
		
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Requerimientos;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Requerimientos']))
		{
			$model->attributes=$_POST['Requerimientos'];
			$model->idUsuarioRequerimiento=Yii::app()->user->getState('idUsuario');
			//exit($model->idUsuarioRequerimiento);
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->idRequerimientos));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Requerimientos']))
		{
			$model->attributes=$_POST['Requerimientos'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idRequerimientos));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	public function actionDeleteUsuarioAsignado($idRequerimiento, $idUsuario) {
		//$model = $this->loadModel($idRequerimiento);
		/*if (!Yii::app()->user->checkAccess('usuarioVinculadoProyecto', array('Proyecto' => $model, 'Funcion' => Proyecto::$LIDERPROYECTO))) {
			throw new CHttpException('404', 'El usuario no es Lider de Proyecto');
		}*/
		$usuarioAsignado=Usuariosasignados::model()->find('idRequerimiento=:idRequerimiento and idUsuario=:idUsuario', array(':idRequerimiento' => $idRequerimiento, ':idUsuario' => $idUsuario));
		//$usuarioProyectoFuncion = UsuarioProyectoFuncion::model()->find('idProyecto=:idProyecto and idFuncion=:idFuncion and idUsuario=:idUsuario', array(':idProyecto' => $idProyecto, ':idFuncion' => $idFuncion, ':idUsuario' => $idUsuario));
		if($usuarioAsignado->idUsuario==Yii::app()->user->getState('idUsuario')){
			$usuarioAsignado->delete();
		}
		else 
			throw new CHttpException('404', 'El usuario no puede borrar a este usuario');
	
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id)
	{
		
		$dataProvider=new CActiveDataProvider('Requerimientos',
				array('criteria'=>array(
						'condition'=>'idProyectoRequerimiento='.$id,
						
				)
				));
		$this->render('index', array(
				//'dataProviderProyecto' => $dataProviderProyecto,
				'dataProvider'=>$dataProvider,
		));
		
		
		
		
		
		/*$aux=Yii::app()->user->getState('idUsuario');
		
		$dataProvider=new CActiveDataProvider('Requerimientos',
		array('criteria'=>array(
				'condition'=>'idUsuarioRequerimiento='.$aux,
				)			
		));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Requerimientos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Requerimientos']))
			$model->attributes=$_GET['Requerimientos'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Requerimientos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='requerimientos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
