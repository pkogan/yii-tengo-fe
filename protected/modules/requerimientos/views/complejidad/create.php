<?php
/* @var $this ComplejidadController */
/* @var $model Complejidad */

$this->breadcrumbs=array(
	'Complejidads'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Complejidad', 'url'=>array('index')),
	array('label'=>'Manage Complejidad', 'url'=>array('admin')),
);
?>

<h1>Create Complejidad</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>