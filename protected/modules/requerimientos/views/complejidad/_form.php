<?php
/* @var $this ComplejidadController */
/* @var $model Complejidad */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'complejidad-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcionComplejidad'); ?>
		<?php echo $form->textField($model,'descripcionComplejidad',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'descripcionComplejidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'puntos'); ?>
		<?php echo $form->textField($model,'puntos'); ?>
		<?php echo $form->error($model,'puntos'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->