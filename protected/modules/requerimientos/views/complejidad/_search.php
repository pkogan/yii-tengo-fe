<?php
/* @var $this ComplejidadController */
/* @var $model Complejidad */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idComplejidad'); ?>
		<?php echo $form->textField($model,'idComplejidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descripcionComplejidad'); ?>
		<?php echo $form->textField($model,'descripcionComplejidad',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'puntos'); ?>
		<?php echo $form->textField($model,'puntos'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->