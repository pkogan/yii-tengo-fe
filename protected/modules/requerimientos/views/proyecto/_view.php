<?php
/* @var $this ProyectoController */
/* @var $data Proyecto */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Proyecto')); ?>:</b>
	<?php echo CHtml::encode($data->Proyecto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->Descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaIncicio')); ?>:</b>
	<?php echo CHtml::encode($data->FechaIncicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaFin')); ?>:</b>
	<?php echo CHtml::encode($data->FechaFin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idEstado')); ?>:</b>
	<?php echo CHtml::encode($data->idEstado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ValorHora')); ?>:</b>
	<?php echo CHtml::encode($data->ValorHora); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('HorasSemanales')); ?>:</b>
	<?php echo CHtml::encode($data->HorasSemanales); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TiempoAjustadoPuntos')); ?>:</b>
	<?php echo CHtml::encode($data->TiempoAjustadoPuntos); ?>
	<br />

	*/ ?>

</div>