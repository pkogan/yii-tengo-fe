<?php
/* @var $this EstadorequerimientoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Estadorequerimientos',
);

$this->menu=array(
	array('label'=>'Create Estadorequerimiento', 'url'=>array('create')),
	array('label'=>'Manage Estadorequerimiento', 'url'=>array('admin')),
);
?>

<h1>Estadorequerimientos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
