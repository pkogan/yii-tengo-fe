<?php
/* @var $this EstadorequerimientoController */
/* @var $data Estadorequerimiento */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idEstado')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idEstado), array('view', 'id'=>$data->idEstado)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DescripcionEstado')); ?>:</b>
	<?php echo CHtml::encode($data->DescripcionEstado); ?>
	<br />


</div>