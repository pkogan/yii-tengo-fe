<?php
/* @var $this EstadorequerimientoController */
/* @var $model Estadorequerimiento */

$this->breadcrumbs=array(
	'Estadorequerimientos'=>array('index'),
	$model->idEstado=>array('view','id'=>$model->idEstado),
	'Update',
);

$this->menu=array(
	array('label'=>'List Estadorequerimiento', 'url'=>array('index')),
	array('label'=>'Create Estadorequerimiento', 'url'=>array('create')),
	array('label'=>'View Estadorequerimiento', 'url'=>array('view', 'id'=>$model->idEstado)),
	array('label'=>'Manage Estadorequerimiento', 'url'=>array('admin')),
);
?>

<h1>Update Estadorequerimiento <?php echo $model->idEstado; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>