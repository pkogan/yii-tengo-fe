<?php
/* @var $this RequerimientosController */
/* @var $data Requerimientos */
?>

<div class="view">

	

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombreRequerimiento')); ?>:</b>
	<?php echo CHtml::encode($data->nombreRequerimiento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actores')); ?>:</b>
	<?php echo CHtml::encode($data->actores); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('costoReal')); ?>:</b>
	<?php echo CHtml::encode($data->costoReal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datosUtilizados')); ?>:</b>
	<?php echo CHtml::encode($data->datosUtilizados); ?>
	<br />

	<b>
	<?php echo 'Complejidad'?>
	<?php /*echo CHtml::encode($data->getAttributeLabel('idComplejidadRequerimiento'))*/; ?>:</b>
	<?php echo CHtml::encode($data->idComplejidadRequerimiento0->descripcionComplejidad); ?>
	<br />

	
	<b>
	<?php echo 'Estado'?>
	<?php /*echo CHtml::encode($data->getAttributeLabel('idEstadoRequerimiento'));*/ ?>:</b>
	<?php echo CHtml::encode($data->idEstadoRequerimiento0->DescripcionEstado); ?>
	<br />

	<b>
	<?php echo 'Lider'?>
	<?php /* echo CHtml::encode($data->getAttributeLabel('idUsuarioRequerimiento')); */?>:</b>
	<?php echo CHtml::encode($data->idUsuarioRequerimiento0->NombreApellido); ?>
	<br />

	<b>
	<?php echo 'Proyecto'?>
	<?php /* echo CHtml::encode($data->getAttributeLabel('idProyectoRequerimiento')); */?>:</b>
	<?php echo CHtml::encode($data->idProyectoRequerimiento); ?>
	<br />
	
	<b><?php /*echo CHtml::encode($data->getAttributeLabel('idRequerimientos'));*/ ?></b>
	<?php echo CHtml::link(CHtml::encode('Detalles'), array('view', 'id'=>$data->idRequerimientos)); ?>
	<br />

	 

</div>