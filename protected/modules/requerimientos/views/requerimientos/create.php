<?php
/* @var $this RequerimientosController */
/* @var $model Requerimientos */

$this->breadcrumbs=array(
	'Requerimientoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Requerimientos', 'url'=>array('index')),
	array('label'=>'Manage Requerimientos', 'url'=>array('admin')),
);
?>

<h1>Create Requerimientos</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>