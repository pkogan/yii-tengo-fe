<?php
/* @var $this RequerimientosController */
/* @var $model Requerimientos */

$this->breadcrumbs=array(
	'Requerimientoses'=>array('index'),
	$model->idRequerimientos=>array('view','id'=>$model->idRequerimientos),
	'Update',
);

$this->menu=array(
	array('label'=>'List Requerimientos', 'url'=>array('index')),
	array('label'=>'Create Requerimientos', 'url'=>array('create')),
	array('label'=>'View Requerimientos', 'url'=>array('view', 'id'=>$model->idRequerimientos)),
	array('label'=>'Manage Requerimientos', 'url'=>array('admin')),
);
?>

<h1>Update Requerimientos <?php echo $model->idRequerimientos; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>