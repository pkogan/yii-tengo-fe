<?php
/* @var $this RequerimientosController */
/* @var $model Requerimientos */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idRequerimientos'); ?>
		<?php echo $form->textField($model,'idRequerimientos'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombreRequerimiento'); ?>
		<?php echo $form->textField($model,'nombreRequerimiento',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'actores'); ?>
		<?php echo $form->textField($model,'actores',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'costoReal'); ?>
		<?php echo $form->textField($model,'costoReal'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datosUtilizados'); ?>
		<?php echo $form->textField($model,'datosUtilizados',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idComplejidadRequerimiento'); ?>
		<?php echo $form->textField($model,'idComplejidadRequerimiento'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idEstadoRequerimiento'); ?>
		<?php echo $form->textField($model,'idEstadoRequerimiento'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idUsuarioRequerimiento'); ?>
		<?php echo $form->textField($model,'idUsuarioRequerimiento'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idProyectoRequerimiento'); ?>
		<?php echo $form->textField($model,'idProyectoRequerimiento'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->