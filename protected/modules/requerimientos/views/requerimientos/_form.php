<?php
/* @var $this RequerimientosController */
/* @var $model Requerimientos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'requerimientos-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombreRequerimiento'); ?>
		<?php echo $form->textField($model,'nombreRequerimiento',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nombreRequerimiento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'actores'); ?>
		<?php echo $form->textField($model,'actores',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'actores'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'costoReal'); ?>
		<?php echo $form->textField($model,'costoReal'); ?>
		<?php echo $form->error($model,'costoReal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datosUtilizados'); ?>
		<?php echo $form->textField($model,'datosUtilizados',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'datosUtilizados'); ?>
	</div>

	<div class="row">
		<?php 
			echo $form->dropDownList($model, 'idComplejidadRequerimiento', CHtml::listData(
			Complejidad::model()->findAll(), 'idComplejidad', 'descripcionComplejidad'),
			array('prompt' => 'Seleccione Complijidad'));
		?>
		<?php /* echo $form->labelEx($model,'idComplejidadRequerimiento'); ?>
		<?php echo $form->textField($model,'idComplejidadRequerimiento'); ?>
		<?php echo $form->error($model,'idComplejidadRequerimiento')*/; ?>
		
		
	</div>

	<div class="row">
		<?php
			echo $form->dropDownList($model, 'idEstadoRequerimiento', CHtml::listData(
			Estadorequerimiento::model()->findAll(), 'idEstado', 'DescripcionEstado'),
			array('prompt' => 'Seleccione Estado'));
		?>
		<?php /*echo $form->labelEx($model,'idEstadoRequerimiento'); ?>
		<?php echo $form->textField($model,'idEstadoRequerimiento'); ?>
		<?php echo $form->error($model,'idEstadoRequerimiento')*/; ?>
	</div>

	<div class="row">
		<?php /* echo $form->labelEx($model,'idUsuarioRequerimiento'); ?>
		<?php echo $form->textField($model,'idUsuarioRequerimiento'); ?>
		<?php echo $form->error($model,'idUsuarioRequerimiento'); */?>
	</div>

	<div class="row">
		<?php 
			echo $form->dropDownList($model, 'idProyectoRequerimiento', CHtml::listData(
			Proyecto::model()->findAll(), 'id', 'Proyecto'),
			array('prompt' => 'Seleccione el proyecto'));
		?>
		<?php /*echo $form->labelEx($model,'idProyectoRequerimiento'); ?>
		<?php echo $form->textField($model,'idProyectoRequerimiento'); ?>
		<?php echo $form->error($model,'idProyectoRequerimiento');*/ ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->