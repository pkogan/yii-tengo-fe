<?php
/* @var $this RequerimientosController */
/* @var $model Requerimientos */

$this->breadcrumbs=array(
	'Requerimientoses'=>array('index','id'=>$model->idProyectoRequerimiento),
	$model->idRequerimientos,
);

$this->menu=array(
	array('label'=>'List Requerimientos', 'url'=>array('index','id'=>$model->idProyectoRequerimiento)),
	array('label'=>'Create Requerimientos', 'url'=>array('create')),
	array('label'=>'Update Requerimientos', 'url'=>array('update', 'id'=>$model->idRequerimientos)),
	array('label'=>'Delete Requerimientos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idRequerimientos),'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>'Manage Requerimientos', 'url'=>array('admin')),
);
?>

<h1>View Requerimientos #<?php echo $model->idRequerimientos; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idRequerimientos',
		'nombreRequerimiento',
		'actores',
		'descripcion',
		'costoReal',
		'datosUtilizados',
		'idComplejidadRequerimiento0.descripcionComplejidad',
		'idEstadoRequerimiento0.DescripcionEstado',
		//'idUsuarioRequerimiento',
		//'idProyectoRequerimiento',
	),
)); ?>

<?php
//echo $model->idRequerimientos;
echo CHtml::link('Asignarce',array('/requerimientos/requerimientos/nuevoUsuarioAsignado','idRequerimiento'=>$model->idRequerimientos,'idUsuario'=>Yii::app()->user->idUsuario)).'<br>';
//echo CHtml::link('Asignarse',array('requerimientos/requerimientos/nuevoUsuarioAsignado', array('idRequerimiento'=>$model->idRequerimientos,'idUsuario'=>Yii::app()->user->idUsuario)));

?>
<?php 
	echo '<br>';
	if (Yii::app()->user->hasFlash('error')): ?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>
<?php 
	echo '<br>';
	if (Yii::app()->user->hasFlash('success')): ?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>


<?php

	$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'proyecto-grid',
    'dataProvider' => $dataprovider,
    'columns' => array(
        'idUsuario0.NombreApellido',
        'idRequerimiento0.nombreRequerimiento',
			array(
			'class' => 'CButtonColumn',
			'template' => '{delete}',
			'buttons' => array(
				'delete' => array(
						'url' => 'Yii::app()->controller->createUrl(\'deleteUsuarioAsignado\', array(\'idRequerimiento\'=>$data["idRequerimiento"],\'idUsuario\'=>$data["idUsuario"]))',
							),
						),
				),	                
    	),
	
	));

?>

