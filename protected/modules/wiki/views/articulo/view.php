<?php
/* @var $this ArticuloController */
/* @var $model Articulo */
/* @var $modelLog Log */

$this->breadcrumbs=array(
	'Articulos'=>array('index','id'=>$model->idProyecto),
	$model->idArticulo,
);

$this->menu=array(
	array('label'=>'Listar Articulos', 'url'=>array('index','id'=>$model->idProyecto)),
	array('label'=>'Crear Articulo', 'url'=>array('create','id'=>$model->idProyecto)),
	array('label'=>'Editar Articulo', 'url'=>array('update', 'id'=>$model->idArticulo)),
	array('label'=>'Eliminar Articulo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idArticulo),'confirm'=>'Are you sure you want to delete this item?')),
);

Yii::app()->clientScript->registerScript('search', "
$('.log-button').click(function(){
	$('.log-grid').toggle();
	return false;
});

");?>

<h1>Vista del art&iacute;culo: "<?php echo $model->titulo; ?>"</h1>
<?php echo $model->texto;
echo "<br /><br />" ?>
<?php echo CHtml::link('Ver log', '#', array('class' => 'log-button')); ?>
<div class='log-grid' style='display:none'>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'articulo-grid',
	'dataProvider'=>$modelLog->search(),
	'filter'=>$modelLog,
	'columns'=>array(
		'titulo',
		'fecha',
		'idUsuario0.NombreApellido',
		array(
			'class'=>'CButtonColumn',
			'template' =>  '{view}',
			'buttons'=>array(
                            'view'=>array(
				'url'=>'Yii::app()->controller->createUrl(\'/wiki/log/view\',array(\'id\'=>$data["idLog"]))'
                                )
		),
	),
			),
)); ?>
</div>
