<?php
/* @var $this ArticuloController */

$this->breadcrumbs=array(
	'Articulo',
);
$this->menu=array(
	array('label'=>'Crear Articulo', 'url'=>array('create','id'=>$idProyecto),'visible'=>Yii::app()->user->checkAccess('administrador')),
);
?>
<h1>Detalle de art&iacute;culos publicados:</h1>

<p>
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
        )); 
    ?>

</p>
