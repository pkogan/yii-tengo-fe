<?php
/* @var $this ArticuloController */
/* @var $model Articulo */

$this->breadcrumbs=array(
	'Articulos'=>array('index','id'=>$idProyecto),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Articulos', 'url'=>array('index','id'=>$idProyecto)),
);
?>

<h1>Create Articulo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>