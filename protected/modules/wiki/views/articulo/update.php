<?php
/* @var $this ArticuloController */
/* @var $model Articulo */

$this->breadcrumbs=array(
	'Articulos'=>array('index','id'=>$model->idProyecto),
	$model->idArticulo=>array('view','id'=>$model->idArticulo),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Articulos', 'url'=>array('index','id'=>$model->idProyecto)),
	array('label'=>'Crear Articulo', 'url'=>array('create')),
	array('label'=>'Ver Articulo', 'url'=>array('view', 'id'=>$model->idArticulo)),
);
?>

<h1>Update Articulo <?php echo $model->idArticulo; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>