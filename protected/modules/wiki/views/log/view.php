<?php
/* @var $this LogController */
/* @var $model Log */
$this->breadcrumbs=array(
	'Articulos'=>array('/wiki/articulo/index','id'=>$model->idArticulo0->idProyecto),
	$model->idLog,
);


?>

<h1>Vista del hist&oacute;rico del &aacute;rticulo: <?php echo $model->idLog; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'idLog',
		'idArticulo',
		'titulo',
		'texto',
		'fecha',
		'idUsuario',
	),
)); ?>
