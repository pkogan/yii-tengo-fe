<?php

class ArticuloController extends Controller {

    public $layout = '//layouts/column2';

    public function actionIndex($id) {
        
        $model=Proyecto::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'El proyecto no existe');
                if(!Yii::app()->user->checkAccess('usuarioVinculadoProyecto',array('Proyecto'=>$model))){
                    throw new CHttpException('El usuario no esta vinculado al Proyecto');
                }
        //$this->render('index');
        $dataProvider = new CActiveDataProvider('Articulo', array(
            'criteria' => array(
                'condition' => 'idProyecto=:id',
                'params' => array(':id' => $id)
            )
        ));

        $this->render('index', array(
            'dataProvider' => $dataProvider,'idProyecto'=>$id
        ));
    }

    // Uncomment the following methods and override them if needed
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    
//	public function actions()
//	{
//		// return external action classes, e.g.:
//		return array(
//			'action1'=>'path.to.ActionClass',
//			'action2'=>array(
//				'class'=>'path.to.AnotherActionClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('view', 'update', 'index'),
                'users' => array("@"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('view', 'update', 'index', 'admin', 'delete', 'create'),
                'roles' => array('administrador'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id) {
    	$modelLog = new Log('search');
    	$model=$this->loadModel($id);
    	$modelLog->unsetAttributes();  // clear any default values
    	if (isset($_GET['Log']))
    		$modelLog->attributes = $_GET['Log'];
    	$modelLog->idArticulo=$id;
    	
    	
    	$artic = Articulo::model()->findAll('idArticulo<>:id', array(':id'=>$id));
    	$titulo=$model->titulo;
    	$texto=$model->texto;
    	foreach ($artic as $key => $titu) {
    		$url=yii::app()->createUrl('/wiki/articulo/view', array('id'=>$titu->idArticulo));
    		$texto= str_replace($titu->titulo, '<a href="'.$url.'">'.strtoupper($titu->titulo).'</a>' , $texto);}
    	
    	$model->titulo=$titulo;
    	$model->texto=$texto;
    	
        $this->render('view', array(
            'model' => $model,
        	'modelLog'=>$modelLog,
        ));
    }

    public function actionAdmin() {
    	
        $model = new Articulo('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Articulo']))
            $model->attributes = $_GET['Articulo'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }
//,$fechaAlta,$idUsuario
    public function actionCreate($id) {
    	$proyecto=Proyecto::model()->findByPk($id);
    	
    	if(is_null($proyecto))
    		throw new CHttpException(404,'El proyecto no existe');
        $model = new Articulo;
        
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Articulo'])) {
            $model->attributes = $_POST['Articulo'];
            $model->idProyecto=$id;
            $model->fecha=date('Y-m-d h:i:s');
            $model->fechaAlta=date('Y-m-d h:i:s');
            $model->idUsuario=Yii::app()->user->getState('idUsuario');
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->idArticulo));
        }

        $this->render('create', array(
            'model' => $model,'idProyecto'=>$id
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Articulo'])) {
            $model->attributes=$_POST['Articulo'];
           
            $modelLog = new Log;
            $modelLog->idArticulo=$id;
            $modelLog->fecha=date('dmY');
            $modelLog->titulo=$model->titulo;
            $modelLog->texto=$model->texto;
            $modelLog->idUsuario=$model->idUsuario;
            if ($modelLog->save()) {
                $model->attributes = $_POST['Articulo'];
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->idArticulo));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
/**
 * 
 * @param type $id
 * @return Articulo
 * @throws CHttpException
 */
    public function loadModel($id) {
        $model = Articulo::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'articulo-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}