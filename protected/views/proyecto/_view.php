<?php
/* @var $this ProyectoController */
/* @var $data Proyecto */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Proyecto')); ?>:</b>
	<?php echo CHtml::encode($data->Proyecto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->Descripcion); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('idEstado0.EstadoProyecto')); ?>:</b>
	<?php echo CHtml::encode($data->idEstado0->EstadoProyecto); ?>
	<br />

</div>