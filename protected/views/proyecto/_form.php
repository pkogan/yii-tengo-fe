<?php
/* @var $this ProyectoController */
/* @var $model Proyecto */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'proyecto-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Proyecto'); ?>
		<?php echo $form->textField($model,'Proyecto',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'Proyecto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Descripcion'); ?>
		<?php echo $form->textArea($model,'Descripcion',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'Descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FechaIncicio'); ?>
		<?php echo $form->textField($model,'FechaIncicio'); ?>
		<?php echo $form->error($model,'FechaIncicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FechaFin'); ?>
		<?php echo $form->textField($model,'FechaFin'); ?>
		<?php echo $form->error($model,'FechaFin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idEstado'); ?>
		<?php echo $form->textField($model,'idEstado'); ?>
		<?php echo $form->error($model,'idEstado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ValorHora'); ?>
		<?php echo $form->textField($model,'ValorHora'); ?>
		<?php echo $form->error($model,'ValorHora'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'HorasSemanales'); ?>
		<?php echo $form->textField($model,'HorasSemanales'); ?>
		<?php echo $form->error($model,'HorasSemanales'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TiempoAjustadoPuntos'); ?>
		<?php echo $form->textField($model,'TiempoAjustadoPuntos'); ?>
		<?php echo $form->error($model,'TiempoAjustadoPuntos'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->