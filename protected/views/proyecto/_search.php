<?php
/* @var $this ProyectoController */
/* @var $model Proyecto */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Proyecto'); ?>
		<?php echo $form->textField($model,'Proyecto',array('size'=>60,'maxlength'=>1000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Descripcion'); ?>
		<?php echo $form->textArea($model,'Descripcion',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FechaIncicio'); ?>
		<?php echo $form->textField($model,'FechaIncicio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FechaFin'); ?>
		<?php echo $form->textField($model,'FechaFin'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idEstado'); ?>
		<?php echo $form->textField($model,'idEstado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ValorHora'); ?>
		<?php echo $form->textField($model,'ValorHora'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'HorasSemanales'); ?>
		<?php echo $form->textField($model,'HorasSemanales'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TiempoAjustadoPuntos'); ?>
		<?php echo $form->textField($model,'TiempoAjustadoPuntos'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->