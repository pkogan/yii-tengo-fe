<?php
/* @var $this ProyectoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Proyectos',
);

$this->menu=array(
	array('label'=>'Create Proyecto', 'url'=>array('create'),'visible'=>Yii::app()->user->checkAccess('administrador')),
	array('label'=>'Manage Proyecto', 'url'=>array('admin'),'visible'=>Yii::app()->user->checkAccess('administrador')),
);
?>

<h1>Proyectos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
