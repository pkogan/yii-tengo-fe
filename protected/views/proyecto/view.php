<?php
/* @var $this ProyectoController */
/* @var $model Proyecto */
/* @var $modelUsuarioProyectoFuncion UsuarioProyectoFuncion */

$this->breadcrumbs = array(
    'Proyectos' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'List Proyecto', 'url' => array('index')),
    array('label' => 'Create Proyecto', 'url' => array('create'), 'visible' => Yii::app()->user->checkAccess('administrador')),
    array('label' => 'Update Proyecto', 'url' => array('update', 'id' => $model->id), 'visible' => Yii::app()->user->checkAccess('usuarioVinculadoProyecto', array('Proyecto' => $model, 'Funcion' => Proyecto::$LIDERPROYECTO))),
    array('label' => 'Delete Proyecto', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?'), 'visible' => Yii::app()->user->checkAccess('administrador')),
    array('label' => 'Manage Proyecto', 'url' => array('admin'), 'visible' => Yii::app()->user->checkAccess('administrador')),
    array('label' => 'Requerimientos', 'url' => array('/requerimientos/default/index', 'id' => $model->id)),
    array('label' => 'Edm', 'url' => array('/edm/edmDocumento/admin', 'id' => $model->id)),
    array('label' => 'Wiki', 'url' => array('/wiki/default/index', 'id' => $model->id)),
    array('label' => 'Planificación', 'url' => array('/planificacion/default/index', 'id' => $model->id)),
    array('label' => 'RRHH', 'url' => array('/rrhh/default/index', 'id' => $model->id)),
    array('label' => 'Métricas', 'url' => array('/metricas/requerimientos/index', 'id' => $model->id)),
    array('label' => 'Casos de Test', 'url' => array('/test/default/index', 'id' => $model->id)),
    array('label' => 'Incidentes', 'url' => array('/incidentes/default/index', 'id' => $model->id)),
);

Yii::app()->clientScript->registerScript('search', "
$('.usuarios-button').click(function(){
	$('.usuarios-form').toggle();
	return false;
});

");
?>

<h1>View Proyecto #<?php echo $model->id; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'Proyecto',
        'Descripcion',
        'FechaIncicio',
        'FechaFin',
        'idEstado0.EstadoProyecto',
//		'ValorHora',
//		'HorasSemanales',
//		'TiempoAjustadoPuntos',
    ),
));
?>
<h2>Usuarios Asignados</h2>
<?php if (Yii::app()->user->hasFlash('usuarioProyecto')): ?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('usuarioProyecto'); ?>
    </div>
<?php endif; ?>
<?php echo CHtml::link('Agregar Nuevo Usuario', '#', array('class' => 'usuarios-button')); ?>

<div class="usuarios-form search-form" style="<?php if (!$modelUsuarioProyectoFuncion->hasErrors()) echo 'display:none'; ?>">
    <?php $this->renderPartial('_formUsuario', array('model' => $modelUsuarioProyectoFuncion)) ?>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'proyecto-grid',
    'dataProvider' => $dataproviderUsuarios,
    'columns' => array(
        'idUsuario0.Usuario',
        'idUsuario0.NombreApellido',
        'idFuncion0.Funcion',
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'url' => 'Yii::app()->controller->createUrl(\'deleteUsuario\', array(\'idProyecto\'=>$data["idProyecto"],\'idFuncion\'=>$data["idFuncion"],\'idUsuario\'=>$data["idUsuario"]))',
                ),
            ),
        ),
    ),
));
?>

