<?php
/* @var $this ProyectoController */
/* @var $model UsuarioProyectoFuncion */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'UsuarioProyectoFuncionj-form',
        'enableAjaxValidation' => false,
            ));
    ?>

    <p class="note">Nuevo Usuario</p>

        <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'idUsuario.Usuario'); ?>
<?php echo $form->dropDownList($model, 'idUsuario', CHtml::listData(Usuario::model()->findAll(), 'id', 'NombreApellido'), array('empty' => 'Seleccionar..')); ?>
        <?php echo $form->error($model, 'idUsuario'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'idFuncion0.Funcion'); ?>
<?php echo $form->dropDownList($model, 'idFuncion', CHtml::listData(Funciones::model()->findAll(), 'id', 'Funcion'), array('empty' => 'Seleccionar..')); ?>
<?php echo $form->error($model, 'idFuncion'); ?>
    </div>

    <div class="row buttons">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->