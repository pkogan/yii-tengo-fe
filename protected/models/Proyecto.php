<?php

/**
 * This is the model class for table "Proyecto".
 *
 * The followings are the available columns in table 'Proyecto':
 * @property integer $id
 * @property string $Proyecto
 * @property string $Descripcion
 * @property string $FechaIncicio
 * @property string $FechaFin
 * @property integer $idEstado
 * @property double $ValorHora
 * @property integer $HorasSemanales
 * @property double $TiempoAjustadoPuntos
 *
 * The followings are the available model relations:
 * @property EstadoProyecto $idEstado0
 * @property UsuarioProyectoFuncion[] $usuarioProyectoFuncions
 * @property Articulo[] $articulos
 * @property EdmDocumento[] $edmDocumentos
 * @property Etapaproyecto[] $etapaproyectos
 * @property Incidentes[] $incidentes
 * @property Requerimientos[] $requerimientoses
 * @property Test[] $tests
 */
class Proyecto extends CActiveRecord
{
	/**
     * constante que refiere al id de la actividad Lider de Proyecto
     */
    static public $LIDERPROYECTO = 1;
    /**
     * constante que refiere al id de la actividad Cliente
     */
    static public $CLIENTE = 5;

    /**
     * Devuelve verdadero si el usuario de $idUsuario cumple la función idFuncion
     * Si idFunción es null devuelve verdadero si el usuario está vinculado al proyecto
     * @param int $idUsuario id del Usuario
     * @param int $idFuncion id de la Función a validar <5
     * @return boolean
     */
    public function usuarioVinculado($idUsuario,$idFuncion=null) {
        if(!is_null($idFuncion)&&  is_int($idFuncion))
            if(0>$idFuncion || $idFuncion>5){
                throw new Exception('Error idFunción inválido');
            }
              
        $usuarioProyecto = false;
        foreach ($this->usuarioProyectoFuncions as $UsuarioFuncion) {
            if ($idUsuario == $UsuarioFuncion->idUsuario && (is_null($idFuncion) || $UsuarioFuncion->idFuncion == $idFuncion)) {
                $usuarioProyecto = true;
                break;
            }
        }
        return $usuarioProyecto;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Proyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Proyecto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Proyecto, Descripcion, FechaIncicio, idEstado, ValorHora, HorasSemanales, TiempoAjustadoPuntos', 'required'),
			array('idEstado, HorasSemanales', 'numerical', 'integerOnly'=>true),
			array('ValorHora, TiempoAjustadoPuntos', 'numerical'),
			array('Proyecto', 'length', 'max'=>1000),
			array('FechaFin', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Proyecto, Descripcion, FechaIncicio, FechaFin, idEstado, ValorHora, HorasSemanales, TiempoAjustadoPuntos', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idEstado0' => array(self::BELONGS_TO, 'EstadoProyecto', 'idEstado'),
			'usuarioProyectoFuncions' => array(self::HAS_MANY, 'UsuarioProyectoFuncion', 'idProyecto'),
			'articulos' => array(self::HAS_MANY, 'Articulo', 'idProyecto'),
			'edmDocumentos' => array(self::HAS_MANY, 'EdmDocumento', 'documento_proyecto'),
			'etapaproyectos' => array(self::HAS_MANY, 'Etapaproyecto', 'idProyecto'),
			'incidentes' => array(self::HAS_MANY, 'Incidentes', 'idProyecto'),
			'requerimientoses' => array(self::HAS_MANY, 'Requerimientos', 'idProyectoRequerimiento'),
			'tests' => array(self::HAS_MANY, 'Test', 'id_proyecto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Proyecto' => 'Proyecto',
			'Descripcion' => 'Descripcion',
			'FechaIncicio' => 'Fecha Incicio',
			'FechaFin' => 'Fecha Fin',
			'idEstado' => 'Id Estado',
			'ValorHora' => 'Valor Hora',
			'HorasSemanales' => 'Horas Semanales',
			'TiempoAjustadoPuntos' => 'Tiempo Ajustado Puntos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Proyecto',$this->Proyecto,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('FechaIncicio',$this->FechaIncicio,true);
		$criteria->compare('FechaFin',$this->FechaFin,true);
		$criteria->compare('idEstado',$this->idEstado);
		$criteria->compare('ValorHora',$this->ValorHora);
		$criteria->compare('HorasSemanales',$this->HorasSemanales);
		$criteria->compare('TiempoAjustadoPuntos',$this->TiempoAjustadoPuntos);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}