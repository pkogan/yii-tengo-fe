<?php

/**
 * This is the model class for table "etapaproyecto".
 *
 * The followings are the available columns in table 'etapaproyecto':
 * @property integer $idEtapa
 * @property integer $idProyecto
 * @property string $nombre
 * @property string $descripcion
 * @property string $fechaInicio
 * @property string $fechaFin
 * @property double $porcentaje
 *
 * The followings are the available model relations:
 * @property Proyecto $idProyecto0
 * @property Usuario[] $usuarios
 */
class Etapaproyecto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Etapaproyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'etapaproyecto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProyecto, nombre, descripcion, fechaInicio,  porcentaje', 'required'),
			array('idProyecto', 'numerical', 'integerOnly'=>true),
			array('porcentaje', 'numerical'),
			array('nombre', 'length', 'max'=>1000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idEtapa, idProyecto, nombre, descripcion, fechaInicio, fechaFin, porcentaje', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProyecto0' => array(self::BELONGS_TO, 'Proyecto', 'idProyecto'),
			'usuarios' => array(self::MANY_MANY, 'Usuario', 'recursoproyecto(idEtapa, idUsuario)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idEtapa' => 'Id Etapa',
			'idProyecto' => 'Id Proyecto',
			'nombre' => 'Nombre',
			'descripcion' => 'Descripcion',
			'fechaInicio' => 'Fecha Inicio',
			'fechaFin' => 'Fecha Fin',
			'porcentaje' => 'Porcentaje',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idEtapa',$this->idEtapa);
		$criteria->compare('idProyecto',$this->idProyecto);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('fechaInicio',$this->fechaInicio,true);
		$criteria->compare('fechaFin',$this->fechaFin,true);
		$criteria->compare('porcentaje',$this->porcentaje);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}