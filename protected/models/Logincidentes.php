<?php

/**
 * This is the model class for table "logincidentes".
 *
 * The followings are the available columns in table 'logincidentes':
 * @property integer $idLog
 * @property integer $idIncidente
 * @property string $fecha
 * @property integer $idUsuario
 * @property string $descripcion
 * @property integer $idEstado
 *
 * The followings are the available model relations:
 * @property Estadolog $idEstado0
 * @property Incidentes $idIncidente0
 * @property Usuario $idUsuario0
 */
class Logincidentes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Logincidentes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'logincidentes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idIncidente, fecha, idUsuario, descripcion, idEstado', 'required'),
			array('idIncidente, idUsuario, idEstado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idLog, idIncidente, fecha, idUsuario, descripcion, idEstado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idEstado0' => array(self::BELONGS_TO, 'Estadolog', 'idEstado'),
			'idIncidente0' => array(self::BELONGS_TO, 'Incidentes', 'idIncidente'),
			'idUsuario0' => array(self::BELONGS_TO, 'Usuario', 'idUsuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idLog' => 'Id Log',
			'idIncidente' => 'Id Incidente',
			'fecha' => 'Fecha',
			'idUsuario' => 'Id Usuario',
			'descripcion' => 'Descripcion',
			'idEstado' => 'Id Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idLog',$this->idLog);
		$criteria->compare('idIncidente',$this->idIncidente);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('idUsuario',$this->idUsuario);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('idEstado',$this->idEstado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}