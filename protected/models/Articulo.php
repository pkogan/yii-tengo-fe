<?php

/**
 * This is the model class for table "articulo".
 *
 * The followings are the available columns in table 'articulo':
 * @property integer $idArticulo
 * @property integer $idProyecto
 * @property string $titulo
 * @property string $texto
 * @property string $fechaAlta
 * @property string $fecha
 * @property integer $idUsuario
 *
 * The followings are the available model relations:
 * @property Proyecto $idProyecto0
 * @property Usuario $idUsuario0
 * @property Log[] $logs
 */
class Articulo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Articulo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'articulo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProyecto, titulo, texto, fechaAlta, fecha, idUsuario', 'required'),
			array('idProyecto, idUsuario', 'numerical', 'integerOnly'=>true),
			array('titulo', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idArticulo, idProyecto, titulo, texto, fechaAlta, fecha, idUsuario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProyecto0' => array(self::BELONGS_TO, 'Proyecto', 'idProyecto'),
			'idUsuario0' => array(self::BELONGS_TO, 'Usuario', 'idUsuario'),
			'logs' => array(self::HAS_MANY, 'Log', 'idArticulo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idArticulo' => 'Id Articulo',
			'idProyecto' => 'Id Proyecto',
			'titulo' => 'Titulo',
			'texto' => 'Texto',
			'fechaAlta' => 'Fecha Alta',
			'fecha' => 'Fecha',
			'idUsuario' => 'Id Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idArticulo',$this->idArticulo);
		$criteria->compare('idProyecto',$this->idProyecto);
		$criteria->compare('titulo',$this->titulo,true);
		$criteria->compare('texto',$this->texto,true);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('idUsuario',$this->idUsuario);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}