<?php

/**
 * This is the model class for table "requerimientos".
 *
 * The followings are the available columns in table 'requerimientos':
 * @property integer $idRequerimientos
 * @property string $nombreRequerimiento
 * @property string $actores
 * @property string $descripcion
 * @property double $costoReal
 * @property string $datosUtilizados
 * @property integer $idComplejidadRequerimiento
 * @property integer $idEstadoRequerimiento
 * @property integer $idUsuarioRequerimiento
 * @property integer $idProyectoRequerimiento
 *
 * The followings are the available model relations:
 * @property Complejidad $idComplejidadRequerimiento0
 * @property Proyecto $idProyectoRequerimiento0
 * @property Estadorequerimiento $idEstadoRequerimiento0
 * @property Usuario $idUsuarioRequerimiento0
 */
class Requerimientos extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Requerimientos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requerimientos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombreRequerimiento, costoReal', 'required'),
			array('idComplejidadRequerimiento, idEstadoRequerimiento, idUsuarioRequerimiento, idProyectoRequerimiento', 'numerical', 'integerOnly'=>true),
			array('costoReal', 'numerical'),
			array('nombreRequerimiento, actores, datosUtilizados', 'length', 'max'=>100),
			array('descripcion', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idRequerimientos, nombreRequerimiento, actores, descripcion, costoReal, datosUtilizados, idComplejidadRequerimiento, idEstadoRequerimiento, idUsuarioRequerimiento, idProyectoRequerimiento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idComplejidadRequerimiento0' => array(self::BELONGS_TO, 'Complejidad', 'idComplejidadRequerimiento'),
			'idProyectoRequerimiento0' => array(self::BELONGS_TO, 'Proyecto', 'idProyectoRequerimiento'),
			'idEstadoRequerimiento0' => array(self::BELONGS_TO, 'Estadorequerimiento', 'idEstadoRequerimiento'),
			'idUsuarioRequerimiento0' => array(self::BELONGS_TO, 'Usuario', 'idUsuarioRequerimiento'),
                        'Usuarios' => array(self::MANY_MANY, 'Usuario', 'usuariosasignados(idRequerimiento, idUsuario)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idRequerimientos' => 'Id Requerimientos',
			'nombreRequerimiento' => 'Nombre Requerimiento',
			'actores' => 'Actores',
			'descripcion' => 'Descripcion',
			'costoReal' => 'Costo Real',
			'datosUtilizados' => 'Datos Utilizados',
			'idComplejidadRequerimiento' => 'Id Complejidad Requerimiento',
			'idEstadoRequerimiento' => 'Id Estado Requerimiento',
			'idUsuarioRequerimiento' => 'Id Usuario Requerimiento',
			'idProyectoRequerimiento' => 'Id Proyecto Requerimiento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idRequerimientos',$this->idRequerimientos);
		$criteria->compare('nombreRequerimiento',$this->nombreRequerimiento,true);
		$criteria->compare('actores',$this->actores,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('costoReal',$this->costoReal);
		$criteria->compare('datosUtilizados',$this->datosUtilizados,true);
		$criteria->compare('idComplejidadRequerimiento',$this->idComplejidadRequerimiento);
		$criteria->compare('idEstadoRequerimiento',$this->idEstadoRequerimiento);
		$criteria->compare('idUsuarioRequerimiento',$this->idUsuarioRequerimiento);
		$criteria->compare('idProyectoRequerimiento',$this->idProyectoRequerimiento);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}