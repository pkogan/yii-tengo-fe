<?php

/**
 * This is the model class for table "incidentes".
 *
 * The followings are the available columns in table 'incidentes':
 * @property integer $idIncidente
 * @property string $nombre
 * @property integer $idUsuario
 * @property integer $idProyecto
 * @property string $fechaAlta
 * @property string $descripcion
 * @property integer $idSeveridad
 * @property integer $idTipo
 *
 * The followings are the available model relations:
 * @property Tipoincidente $idTipo0
 * @property Usuario $idUsuario0
 * @property Proyecto $idProyecto0
 * @property Severidadincidente $idSeveridad0
 * @property Logincidentes[] $logincidentes
 * @property Severidadincidente $severidadincidente
 * @property Tipoincidente $tipoincidente
 */
class Incidentes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Incidentes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'incidentes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, idUsuario, idProyecto, fechaAlta, descripcion, idSeveridad, idTipo', 'required'),
			array('idUsuario, idProyecto, idSeveridad, idTipo', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>25),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idIncidente, nombre, idUsuario, idProyecto, fechaAlta, descripcion, idSeveridad, idTipo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idTipo0' => array(self::BELONGS_TO, 'Tipoincidente', 'idTipo'),
			'idUsuario0' => array(self::BELONGS_TO, 'Usuario', 'idUsuario'),
			'idProyecto0' => array(self::BELONGS_TO, 'Proyecto', 'idProyecto'),
			'idSeveridad0' => array(self::BELONGS_TO, 'Severidadincidente', 'idSeveridad'),
			'logincidentes' => array(self::HAS_MANY, 'Logincidentes', 'idIncidente'),
			'severidadincidente' => array(self::HAS_ONE, 'Severidadincidente', 'id'),
			'tipoincidente' => array(self::HAS_ONE, 'Tipoincidente', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idIncidente' => 'Id Incidente',
			'nombre' => 'Nombre',
			'idUsuario' => 'Id Usuario',
			'idProyecto' => 'Id Proyecto',
			'fechaAlta' => 'Fecha Alta',
			'descripcion' => 'Descripcion',
			'idSeveridad' => 'Id Severidad',
			'idTipo' => 'Id Tipo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idIncidente',$this->idIncidente);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('idUsuario',$this->idUsuario);
		$criteria->compare('idProyecto',$this->idProyecto);
		$criteria->compare('fechaAlta',$this->fechaAlta,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('idSeveridad',$this->idSeveridad);
		$criteria->compare('idTipo',$this->idTipo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}