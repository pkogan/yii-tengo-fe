<?php

/**
 * This is the model class for table "edm_documento".
 *
 * The followings are the available columns in table 'edm_documento':
 * @property integer $documento_id
 * @property string $documento_nombre
 * @property string $documento_archivo
 * @property string $documento_fechayhora
 * @property integer $documento_usuario
 * @property integer $documento_privilegios
 * @property string $documento_descripcion
 * @property integer $documento_activo
 * @property integer $documento_proyecto
 *
 * The followings are the available model relations:
 * @property EdmCambios[] $edmCambioses
 * @property Proyecto $documentoProyecto
 * @property Usuario $documentoUsuario
 */
class EdmDocumento extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EdmDocumento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'edm_documento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('documento_nombre, documento_archivo, documento_fechayhora, documento_usuario, documento_privilegios, documento_descripcion, documento_activo, documento_proyecto', 'required'),
			array('documento_usuario, documento_privilegios, documento_activo, documento_proyecto', 'numerical', 'integerOnly'=>true),
			array('documento_nombre', 'length', 'max'=>255),
			array('documento_archivo', 'length', 'max'=>100),
			array('documento_descripcion', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('documento_id, documento_nombre, documento_archivo, documento_fechayhora, documento_usuario, documento_privilegios, documento_descripcion, documento_activo, documento_proyecto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'edmCambioses' => array(self::HAS_MANY, 'EdmCambios', 'cambio_documento'),
			'documentoProyecto' => array(self::BELONGS_TO, 'Proyecto', 'documento_proyecto'),
			'documentoUsuario' => array(self::BELONGS_TO, 'Usuario', 'documento_usuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'documento_id' => 'Documento',
			'documento_nombre' => 'Documento Nombre',
			'documento_archivo' => 'Documento Archivo',
			'documento_fechayhora' => 'Documento Fechayhora',
			'documento_usuario' => 'Documento Usuario',
			'documento_privilegios' => 'Documento Privilegios',
			'documento_descripcion' => 'Documento Descripcion',
			'documento_activo' => 'Documento Activo',
			'documento_proyecto' => 'Documento Proyecto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('documento_id',$this->documento_id);
		$criteria->compare('documento_nombre',$this->documento_nombre,true);
		$criteria->compare('documento_archivo',$this->documento_archivo,true);
		$criteria->compare('documento_fechayhora',$this->documento_fechayhora,true);
		$criteria->compare('documento_usuario',$this->documento_usuario);
		$criteria->compare('documento_privilegios',$this->documento_privilegios);
		$criteria->compare('documento_descripcion',$this->documento_descripcion,true);
		$criteria->compare('documento_activo',$this->documento_activo);
		$criteria->compare('documento_proyecto',$this->documento_proyecto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}