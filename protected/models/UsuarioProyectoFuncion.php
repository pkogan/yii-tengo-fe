<?php

/**
 * This is the model class for table "UsuarioProyectoFuncion".
 *
 * The followings are the available columns in table 'UsuarioProyectoFuncion':
 * @property integer $idUsuario
 * @property integer $idProyecto
 * @property integer $idFuncion
 *
 * The followings are the available model relations:
 * @property Proyecto $idProyecto0
 * @property Usuario $idUsuario0
 * @property Funciones $idFuncion0
 */
class UsuarioProyectoFuncion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UsuarioProyectoFuncion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'UsuarioProyectoFuncion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUsuario, idProyecto, idFuncion', 'required'),
			array('idUsuario, idProyecto, idFuncion', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idUsuario, idProyecto, idFuncion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProyecto0' => array(self::BELONGS_TO, 'Proyecto', 'idProyecto'),
			'idUsuario0' => array(self::BELONGS_TO, 'Usuario', 'idUsuario'),
			'idFuncion0' => array(self::BELONGS_TO, 'Funciones', 'idFuncion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idUsuario' => 'Id Usuario',
			'idProyecto' => 'Id Proyecto',
			'idFuncion' => 'Id Funcion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idUsuario',$this->idUsuario);
		$criteria->compare('idProyecto',$this->idProyecto);
		$criteria->compare('idFuncion',$this->idFuncion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}