<?php

/**
 * This is the model class for table "Usuario".
 *
 * The followings are the available columns in table 'Usuario':
 * @property string $Usuario
 * @property integer $id
 * @property string $Clave
 * @property integer $idRol
 * @property string $NombreApellido
 * @property string $Email
 *
 * The followings are the available model relations:
 * @property Rol $idRol0
 * @property UsuarioProyectoFuncion[] $usuarioProyectoFuncions
 * @property Articulo[] $articulos
 * @property Cambioestados[] $cambioestadoses
 * @property EdmCambios[] $edmCambioses
 * @property EdmDocumento[] $edmDocumentos
 * @property Incidentes[] $incidentes
 * @property Log[] $logs
 * @property Logincidentes[] $logincidentes
 * @property Etapaproyecto[] $etapaproyectos
 * @property Requerimientos[] $requerimientoses
 * @property Test[] $tests
 * @property Usuariosasignados[] $usuariosasignadoses
 */
class Usuario extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Usuario, Clave, idRol, NombreApellido, Email', 'required'),
			array('idRol', 'numerical', 'integerOnly'=>true),
			array('Usuario, NombreApellido, Email', 'length', 'max'=>255),
			array('Clave', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('Usuario, id, Clave, idRol, NombreApellido, Email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idRol0' => array(self::BELONGS_TO, 'Rol', 'idRol'),
			'usuarioProyectoFuncions' => array(self::HAS_MANY, 'UsuarioProyectoFuncion', 'idUsuario'),
			'articulos' => array(self::HAS_MANY, 'Articulo', 'idUsuario'),
			'cambioestadoses' => array(self::HAS_MANY, 'Cambioestados', 'id_usuario'),
			'edmCambioses' => array(self::HAS_MANY, 'EdmCambios', 'cambio_usuario'),
			'edmDocumentos' => array(self::HAS_MANY, 'EdmDocumento', 'documento_usuario'),
			'incidentes' => array(self::HAS_MANY, 'Incidentes', 'idUsuario'),
			'logs' => array(self::HAS_MANY, 'Log', 'idUsuario'),
			'logincidentes' => array(self::HAS_MANY, 'Logincidentes', 'idUsuario'),
			'etapaproyectos' => array(self::MANY_MANY, 'Etapaproyecto', 'recursoproyecto(idUsuario, idEtapa)'),
			'requerimientoses' => array(self::HAS_MANY, 'Requerimientos', 'idUsuarioRequerimiento'),
			'tests' => array(self::HAS_MANY, 'Test', 'id_usuario_registro'),
			'requerimientosAsignados' => array(self::MANY_MANY, 'Requerimientos', 'usuariosasignados(idUsuario, idRequerimiento)')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Usuario' => 'Usuario',
			'id' => 'ID',
			'Clave' => 'Clave',
			'idRol' => 'Id Rol',
			'NombreApellido' => 'Nombre Apellido',
			'Email' => 'Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Usuario',$this->Usuario,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('Clave',$this->Clave,true);
		$criteria->compare('idRol',$this->idRol);
		$criteria->compare('NombreApellido',$this->NombreApellido,true);
		$criteria->compare('Email',$this->Email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}