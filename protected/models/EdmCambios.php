<?php

/**
 * This is the model class for table "edm_cambios".
 *
 * The followings are the available columns in table 'edm_cambios':
 * @property integer $cambio_id
 * @property integer $cambio_documento
 * @property string $cambio_nombre
 * @property string $cambio_archivo
 * @property string $cambio_fechayhora
 * @property integer $cambio_usuario
 * @property integer $cambio_privilegios
 * @property string $cambio_descripcion
 * @property integer $cambio_activo
 *
 * The followings are the available model relations:
 * @property EdmDocumento $cambioDocumento
 * @property Usuario $cambioUsuario
 */
class EdmCambios extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EdmCambios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'edm_cambios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cambio_documento, cambio_nombre, cambio_archivo, cambio_fechayhora, cambio_usuario, cambio_privilegios, cambio_descripcion, cambio_activo', 'required'),
			array('cambio_documento, cambio_usuario, cambio_privilegios, cambio_activo', 'numerical', 'integerOnly'=>true),
			array('cambio_nombre', 'length', 'max'=>255),
			array('cambio_archivo', 'length', 'max'=>100),
			array('cambio_descripcion', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('cambio_id, cambio_documento, cambio_nombre, cambio_archivo, cambio_fechayhora, cambio_usuario, cambio_privilegios, cambio_descripcion, cambio_activo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cambioDocumento' => array(self::BELONGS_TO, 'EdmDocumento', 'cambio_documento'),
			'cambioUsuario' => array(self::BELONGS_TO, 'Usuario', 'cambio_usuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cambio_id' => 'Cambio',
			'cambio_documento' => 'Cambio Documento',
			'cambio_nombre' => 'Cambio Nombre',
			'cambio_archivo' => 'Cambio Archivo',
			'cambio_fechayhora' => 'Cambio Fechayhora',
			'cambio_usuario' => 'Cambio Usuario',
			'cambio_privilegios' => 'Cambio Privilegios',
			'cambio_descripcion' => 'Cambio Descripcion',
			'cambio_activo' => 'Cambio Activo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cambio_id',$this->cambio_id);
		$criteria->compare('cambio_documento',$this->cambio_documento);
		$criteria->compare('cambio_nombre',$this->cambio_nombre,true);
		$criteria->compare('cambio_archivo',$this->cambio_archivo,true);
		$criteria->compare('cambio_fechayhora',$this->cambio_fechayhora,true);
		$criteria->compare('cambio_usuario',$this->cambio_usuario);
		$criteria->compare('cambio_privilegios',$this->cambio_privilegios);
		$criteria->compare('cambio_descripcion',$this->cambio_descripcion,true);
		$criteria->compare('cambio_activo',$this->cambio_activo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}