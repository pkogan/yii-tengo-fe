<?php

/**
 * This is the model class for table "requerimientos".
 *
 * The followings are the available columns in table 'requerimientos':
 * @property integer $idRequerimientos
 * @property string $nombreRequerimiento
 * @property string $actores
 * @property string $descripcion
 * @property double $costoReal
 * @property string $datosUtilizados
 * @property integer $idComplejidadRequerimiento
 * @property integer $idEstadoRequerimiento
 * @property integer $idUsuarioRequerimiento
 * @property integer $idProyectoRequerimiento
 */
/**
 * propiedades de datos calculados para metricas de estimacion
 * @property integer costoEstimadoReqHoras Costo estimado del requerimiento en horas (CERH) = puntos * tasa del proyecto
 * @property integer $ceph  Costo estimado proyecto en horas (CEPH)= suma de todos los CERH del proyecto
 * @property float $coeppesos Costo estimado del proyecto en $ = CEPH * valor hora
 * @property integer $Tiempoepsemanas  Duración estimada del proyecto semanas = CEPH / Horas semana
 * @property integer $idProyectoRequerimiento

*/
class RequerimientosMetricas extends Requerimientos
{   
    	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Requerimientos the static model class
	 */

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
 
 	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
                $relacionespadre=parent::relations();
                $relacionespadre['idProyectoRequerimiento0'] = array(self::BELONGS_TO, 'ProyectoMetricas', 'idProyectoRequerimiento');
                return $relacionespadre;
		
	}
        
    /**
    * Costo estimado del Requerimiento en horas (CERH) = puntos * tasa del proyecto
    * @return integer Devuelve el costo estimado del Requerimiento en horas
    */

    
    public function costoEstimadoReqHoras() {


        $puntos=$this->idComplejidadRequerimiento0->puntos;
        $costoEstimadoRHoras=$puntos*$this->idProyectoRequerimiento0->TiempoAjustadoPuntos;        
        return $costoEstimadoRHoras;
    }
 
}