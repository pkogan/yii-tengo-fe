<?php

/**
 * This is the model class for table "Proyecto".
 *
 * The followings are the available columns in table 'Proyecto':
 * @property integer $id
 * @property string $Proyecto
 * @property string $Descripcion
 * @property string $FechaIncicio
 * @property string $FechaFin
 * @property integer $idEstado
 * @property double $ValorHora
 * @property integer $HorasSemanales
 * @property integer $TiempoAjustadoPuntos
 *
 * The followings are the available model relations:
 * @property EstadoProyecto $idEstado0
 * @property UsuarioProyectoFuncion[] $usuarioProyectoFuncions
 */
class ProyectoMetricas extends Proyecto
{
   public static function model($className=__CLASS__)
    {
		return parent::model($className);
    }
 
 	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
                $relacionespadre=parent::relations();
                
                $relacionespadre['requerimientoses'] = array(self::HAS_MANY, 'RequerimientosMetricas', 'idProyectoRequerimiento');
                return $relacionespadre;
		
	}
   
  /**
    * Costo estimado proyecto en horas (CEPH)= suma de todos los costos Estimados del Requerimiento en horas
    * costoEstimadoReqHoras del requerimiento
    * @return integer Devuelve el costo estimado del Proyecto en horas
    */
    public function costoEstimadoProyHoras(){
        $coeph=0;
        foreach ($this->requerimientoses as $proyectoRequerimiento) {
            
            $coeph = $coeph+$proyectoRequerimiento->costoEstimadoReqHoras();
           
            }
         return $coeph;    
     }

/**
    * Costo estimado del proyecto en $ = CEPH * valor hora  costo Estimado del Proyecto en Horas
    * 
    * @return integer Devuelve el costo estimado del Proyecto en horas
    */

     public function costoEstimadoProyPesos() {

        $coePPesos=0;
        $coePPesos=$coePPesos+ ($this->costoEstimadoProyHoras()*$this->ValorHora);
        
        return $coePPesos;
 
    }
/**
    * Duración estimada del proyecto semanas = CEPH / Horas semana
    * Devuelve Falso si HorasSemanales es cero.
    * @return falso si horasSemanales es cero sino la duraciond del Proyecto expresadas en Semana
 */
 
  
    
     public function duracionProySemanas() {

        $duracionSemanas=false;
        if ($this->HorasSemanales <> 0 ){
            $duracionSemanas=$this->costoEstimadoProyHoras()/$this->HorasSemanales;
        }  
        
        return $duracionSemanas;
 
    }  
    
        public function costoRealProyHoras(){
        $corph=0;
        foreach ($this->requerimientoses as $proyectoRequerimiento) {
            
            $corph = $corph+$proyectoRequerimiento->costoReal;
           
            }
         return $corph;    
     }
    

}