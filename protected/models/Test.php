<?php

/**
 * This is the model class for table "test".
 *
 * The followings are the available columns in table 'test':
 * @property integer $id
 * @property integer $id_proyecto
 * @property string $descripcion
 * @property string $resultadoesperado
 * @property string $pasos
 * @property string $prioridad
 * @property integer $id_tipo
 * @property string $fechahora
 * @property integer $id_usuario_registro
 *
 * The followings are the available model relations:
 * @property Cambioestados[] $cambioestadoses
 * @property Proyecto $idProyecto
 * @property Tipotest $idTipo
 * @property Usuario $idUsuarioRegistro
 */
class Test extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Test the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'test';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_proyecto, prioridad, id_tipo, fechahora, id_usuario_registro', 'required'),
			array('id_proyecto, id_tipo, id_usuario_registro', 'numerical', 'integerOnly'=>true),
			array('descripcion, resultadoesperado, pasos', 'length', 'max'=>250),
			array('prioridad', 'length', 'max'=>25),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_proyecto, descripcion, resultadoesperado, pasos, prioridad, id_tipo, fechahora, id_usuario_registro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cambioestadoses' => array(self::HAS_MANY, 'Cambioestados', 'id_test'),
			'idProyecto' => array(self::BELONGS_TO, 'Proyecto', 'id_proyecto'),
			'idTipo' => array(self::BELONGS_TO, 'Tipotest', 'id_tipo'),
			'idUsuarioRegistro' => array(self::BELONGS_TO, 'Usuario', 'id_usuario_registro'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_proyecto' => 'Id Proyecto',
			'descripcion' => 'Descripcion',
			'resultadoesperado' => 'Resultadoesperado',
			'pasos' => 'Pasos',
			'prioridad' => 'Prioridad',
			'id_tipo' => 'Id Tipo',
			'fechahora' => 'Fechahora',
			'id_usuario_registro' => 'Id Usuario Registro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_proyecto',$this->id_proyecto);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('resultadoesperado',$this->resultadoesperado,true);
		$criteria->compare('pasos',$this->pasos,true);
		$criteria->compare('prioridad',$this->prioridad,true);
		$criteria->compare('id_tipo',$this->id_tipo);
		$criteria->compare('fechahora',$this->fechahora,true);
		$criteria->compare('id_usuario_registro',$this->id_usuario_registro);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}